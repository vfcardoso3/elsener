package br.com.associaplus.associapp.helper.abstracts;

import br.com.associaplus.associapp.domain.common.helper.SerializationHelper;
import com.google.gson.JsonSerializer;

/**
 * Created by vfcar on 23/07/2017.
 */
public abstract class AuditableEntity<T> extends AbstractEntity implements JsonSerializer<T> {
    public String toJsonString() {
        return SerializationHelper.getGsonBuilder()
                .registerTypeAdapter(this.getClass(), this)
                .create()
                .toJson(this);
    }
}
