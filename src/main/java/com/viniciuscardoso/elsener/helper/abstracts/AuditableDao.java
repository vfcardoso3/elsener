package br.com.associaplus.associapp.helper.abstracts;

import br.com.associaplus.associapp.domain.common.enumerator.AuditoriaOperacao;
import br.com.associaplus.associapp.domain.config.Auditoria;
import com.viniciuscardoso.arch.vraptor.domain.interfaces.IActorAudited;
import com.viniciuscardoso.arch.vraptor.domain.interfaces.IAuditable;
import com.viniciuscardoso.arch.vraptor.exception.DaoException;
import org.hibernate.Session;
import org.joda.time.LocalDateTime;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by vfcar on 23/07/2017.
 */
public class AuditableDao<T extends AuditableEntity, A extends IActorAudited> extends AbstractAuditableDao<T, A> {

    private Class<T> classe;
    @SuppressWarnings("unchecked")
    public AuditableDao(Session session, Class<? extends AbstractAuditableDao<T, A>> clazz) {
        super(session, clazz);
        this.classe = (Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("unchecked")
    @Override
    public void save(T entity, A creator) {
        try {
            if(entity instanceof IAuditable) {
                ((IAuditable)entity).setCreatedAt(new LocalDateTime());
                ((IAuditable)entity).setCreatedBy(creator);
            }

            super.getSession().getTransaction().begin();
            super.getSession().save(entity);
            super.getSession().getTransaction().commit();
            super.getLogger().info("Objeto [" + this.getEntityName() + "] adicionado com id = " + entity.getId());

            //Auditoria
            Auditoria auditoria = new Auditoria();
            auditoria.setActorAudited(creator);
            auditoria.setOperacao(AuditoriaOperacao.INSERT);
            auditoria.setDtHrEvento(new LocalDateTime());
            auditoria.setNomeObjeto(this.getEntityName());
            auditoria.setIdObjeto(entity.getId());
            auditoria.setJsonObjetoNovo(super.getById(entity.getId()).toJsonString());

            super.getSession().getTransaction().begin();
            super.getSession().save(auditoria);
            super.getSession().getTransaction().commit();

        } catch (Exception e) {
            super.getSession().getTransaction().rollback();
            super.getLogger().info("Erro ao adicionar objeto. " + e.getMessage());
            throw new DaoException("Não foi possível criar o objeto [" + this.getEntityName() + "].", e.getCause());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void save(List<T> lista, A creator) {
        try {
            for (T entity : lista) {
                this.save(entity, creator);
            }
            super.getLogger().info("Objetos [" + this.getEntityName() + "] adicionados com sucesso.");
        } catch (Exception e) {
            super.getSession().getTransaction().rollback();
            super.getLogger().info("Erro ao adicionar objeto. " + e.getMessage());
            throw new DaoException("Não foi possível criar o objeto [" + this.getEntityName() + "].", e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void update(T entity, A changer) {
        try {
            T oldEntity = (T) super.getSession().get(this.classe, entity.getId());
            if(entity instanceof IAuditable) {
                ((IAuditable)entity).setCreatedAt(((IAuditable)oldEntity).getCreatedAt());
                ((IAuditable)entity).setCreatedBy(((IAuditable)oldEntity).getCreatedBy());
                ((IAuditable)entity).setChangedAt(new LocalDateTime());
                ((IAuditable)entity).setChangedBy(changer);
            }

            String oldEntityJson = oldEntity.toJsonString();

            super.getSession().getTransaction().begin();
            super.getSession().merge(entity);
            super.getSession().getTransaction().commit();
            super.getLogger().info("Objeto [" + this.getEntityName() + "] atualizado com id = " + entity.getId());

            //Auditoria
            Auditoria auditoria = new Auditoria();
            auditoria.setActorAudited(changer);
            auditoria.setOperacao(AuditoriaOperacao.UPDATE);
            auditoria.setDtHrEvento(new LocalDateTime());
            auditoria.setNomeObjeto(this.getEntityName());
            auditoria.setIdObjeto(entity.getId());
            auditoria.setJsonObjetoNovo(super.getById(entity.getId()).toJsonString());
            auditoria.setJsonObjetoOld(oldEntityJson);

            super.getSession().getTransaction().begin();
            super.getSession().save(auditoria);
            super.getSession().getTransaction().commit();

        } catch (Exception var4) {
            super.getSession().getTransaction().rollback();
            super.getLogger().info("Erro ao atualizar objeto. " + var4.getMessage());
            throw new DaoException("Não foi possível atualizar o objeto [" + this.getEntityName() + "].", var4);
        }
    }

    @SuppressWarnings("unchecked")
    public void remove(T entity, A deleter) {
        try {
            T oldEntity = (T) super.getSession().get(this.classe, entity.getId());
            String oldEntityJson = oldEntity.toJsonString();

            //Auditoria
            Auditoria auditoria = new Auditoria();
            auditoria.setActorAudited(deleter);
            auditoria.setOperacao(AuditoriaOperacao.DELETE);
            auditoria.setDtHrEvento(new LocalDateTime());
            auditoria.setNomeObjeto(this.getEntityName());
            auditoria.setIdObjeto(entity.getId());
            auditoria.setJsonObjetoNovo("{}");
            auditoria.setJsonObjetoOld(oldEntityJson);

            super.getSession().getTransaction().begin();
            super.getSession().delete(entity);
            super.getSession().getTransaction().commit();
            super.getLogger().info("Objeto [" + this.getEntityName() + "] removido com id = " + entity.getId());

            super.getSession().getTransaction().begin();
            super.getSession().save(auditoria);
            super.getSession().getTransaction().commit();

        } catch (Exception var3) {
            super.getSession().getTransaction().rollback();
            super.getLogger().info("Erro ao atualizar objeto. " + var3.getMessage());
            throw new DaoException("Não foi possível excluir objeto [" + this.getEntityName() + "].", var3);
        }
    }

    private String getEntityName() {
        return this.classe.getSimpleName();
    }
}
