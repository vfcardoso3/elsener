package br.com.associaplus.associapp.helper.utility;

import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.interceptor.download.InputStreamDownload;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTables;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DtWrapper;
import com.viniciuscardoso.arch.vraptor.utility.ConvertUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Objects;

/**
 * Created by vfcar on 23/07/2017.
 */
public abstract class ExportHelper<T extends DtWrapper> {

    private Class<T> clazz;

    @SuppressWarnings("unchecked")
    public ExportHelper() throws IllegalAccessException, InstantiationException {
        this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private ByteArrayOutputStream listExport(DataTables<T> dataTables) throws IllegalAccessException, InstantiationException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        HSSFWorkbook w = new HSSFWorkbook();
        Sheet sheet = w.createSheet(clazz.getSimpleName());
        Row row = sheet.createRow(0);

        for(int i = 0; i < this.clazz.getDeclaredFields().length; i++) {
            row.createCell(i).setCellValue(this.clazz.getDeclaredFields()[i].getName().toUpperCase());
        }

        //Cellstyles
        CreationHelper ch = w.getCreationHelper();
        HSSFCellStyle cs = w.createCellStyle();
        HSSFCellStyle csDate = w.createCellStyle();
        HSSFCellStyle csDateTime = w.createCellStyle();
        HSSFCellStyle csDouble = w.createCellStyle();
        HSSFCellStyle csLong = w.createCellStyle();
        cs.setVerticalAlignment(org.apache.poi.ss.usermodel.CellStyle.VERTICAL_CENTER);

        csDate.cloneStyleFrom(cs);
        csDate.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy"));
        csDateTime.cloneStyleFrom(cs);
        csDateTime.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));
        csDouble.cloneStyleFrom(cs);
        csDouble.setDataFormat(ch.createDataFormat().getFormat("#.##0,00"));
        csLong.cloneStyleFrom(cs);
        csLong.setDataFormat(ch.createDataFormat().getFormat("0"));

        int rowCounter = 1;
        int columnCounter = 0;
        for (T obj : dataTables.getData()) {
            Row dataRow = sheet.createRow(rowCounter);
            for (int i = 0; i < obj.getClass().getDeclaredFields().length; i++) {
                Field field = obj.getClass().getDeclaredFields()[i];
                field.setAccessible(true);

                if (field.getType() == Double.class || field.getType() == double.class) {
                    Cell c = dataRow.createCell(columnCounter);
                    c.setCellStyle(csDouble);
                    c.setCellValue((Double)field.get(obj));
                } else if (field.getType() == Long.class || field.getType() == long.class) {
                    Cell c = dataRow.createCell(columnCounter);
                    c.setCellStyle(csLong);
                    c.setCellValue((Long)field.get(obj));
                } else if (field.getType() == Integer.class || field.getType() == int.class) {
                    Cell c = dataRow.createCell(columnCounter);
                    c.setCellStyle(csLong);
                    c.setCellValue((Integer)field.get(obj));
                } else if (field.getName().toLowerCase().startsWith("dthr")) {
                    Cell c = dataRow.createCell(columnCounter);
                    String maybeDate = (String)field.get(obj);
                    if (maybeDate != null && !Objects.equals(maybeDate, "") && !Objects.equals(maybeDate, "-")) {
                        c.setCellStyle(csDateTime);
                        c.setCellValue(ConvertUtils.parseDate(maybeDate, "yyyy-MM-dd HH:mm:ss").toDateTime().toDate());
                    } else {
                        c.setCellValue(maybeDate);
                    }
                } else if (field.getName().toLowerCase().startsWith("dt") && !field.getName().toLowerCase().startsWith("dt_")) {
                    Cell c = dataRow.createCell(columnCounter);
                    String maybeDate = (String)field.get(obj);
                    maybeDate = maybeDate != null && maybeDate.length() > 10 ? maybeDate.substring(0, 10) : maybeDate;
                    if (maybeDate != null && !Objects.equals(maybeDate, "") && !Objects.equals(maybeDate, "-")) {
                        c.setCellStyle(csDate);
                        c.setCellValue(ConvertUtils.parseDate(maybeDate, "yyyy-MM-dd").toDateTime().toDate());
                    } else {
                        c.setCellValue(maybeDate);
                    }
                } else {
                    dataRow.createCell(columnCounter).setCellValue((String)field.get(obj));
                }
                columnCounter++;
            }
            rowCounter++;
            columnCounter = 0;
        }

        for(int i = 0; i < this.clazz.getDeclaredFields().length; i++) {
            sheet.autoSizeColumn(i);
        }

        w.write(baos);
        baos.close();

        return baos;
    }

    public Download generateDownload(DataTables<T> dataTables) throws IllegalAccessException, IOException, InstantiationException {
        ByteArrayOutputStream baos = this.listExport(dataTables);
        InputStream is = new ByteArrayInputStream(baos.toByteArray());
        return new InputStreamDownload(is, "application/vnd.ms-excel",
                this.clazz.getSimpleName().toLowerCase() + "-grid-export.xls", false, baos.toByteArray().length);
    }
}
