package br.com.associaplus.associapp.helper.interfaces;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vfcar on 23/07/2017.
 */
public interface InactivatableDao<T extends Serializable> {
    List<T> listAtivosSortedBy(String... fields); //ToDo: Refatorar e criar hierarquia abstrata e mover esse método para uma classe acima de AbstractDao<T>
}
