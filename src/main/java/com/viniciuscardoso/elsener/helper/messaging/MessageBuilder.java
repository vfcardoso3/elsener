package br.com.associaplus.associapp.helper.messaging;

import org.joda.time.LocalTime;

/**
 * Created by vfcar on 25/07/2017.
 */
public class MessageBuilder {
    private static String getGreeting() {
        LocalTime hora = new LocalTime();
        if (hora.isAfter(new LocalTime(6,0)) && hora.isBefore(new LocalTime(12, 0))) {
            return "Bom dia!";
        } else if (hora.isAfter(new LocalTime(12,0)) && hora.isBefore(new LocalTime(18,0))) {
            return "Boa tarde!";
        } else {
            return "Boa noite!";
        }
    }

    public static String userForgottenPassword(String hyperlink, String nomeSistema) {
        StringBuilder s = new StringBuilder();
        s.append(MessageBuilder.getGreeting());
        s.append("<br/>");
        s.append("<br/>");
        s.append("Você solicitou recadastramento de sua senha no sistema ").append(nomeSistema).append("<br/>");
        s.append("Clique no link abaixo para ser redirecionado para uma página onde você poderá recriar a senha.");
        s.append("<br/>");
        s.append("<br/>");
        s.append("<br/>");
        s.append("<a href=\"" + hyperlink +"\" target=\"_blank\" rel=\"noopener\">" + hyperlink + "</a>");
        return s.toString();
    }

    public static String welcomeNewUser(String login, String senha, String appAddress, String nomeSistema) {
        StringBuilder s = new StringBuilder();
        s.append(MessageBuilder.getGreeting());
        s.append("<br/>");
        s.append("<br/>");
        s.append("Informamos que sua solicitação de cadastro foi efetuada no sistema ").append(nomeSistema).append(".<br/>");
        s.append("<br/>");
        s.append("Seguem abaixo os dados de acesso ao sistema: ");
        s.append("<br/>");
        s.append("<br/>");
        s.append("-------------").append("<br/>");
        s.append("Endereço: ").append(appAddress).append("<br/>");
        s.append("Usuário: ").append(login).append("<br/>");
        s.append("Senha: ").append(senha).append("<br/>");
        s.append("-------------");
        s.append("<br/>");
        s.append("<br/>");
        s.append("<em>Lembrando que esta senha é provisória. Favor alterá-la assim que possível.</em>");
        return s.toString();
    }

}
