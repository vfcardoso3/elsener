package br.com.associaplus.associapp.helper.transients.datatables;

import com.viniciuscardoso.arch.vraptor.helper.datatables.DtWrapper;
import com.viniciuscardoso.arch.vraptor.utility.ConvertUtils;

import java.util.Objects;

/**
 * Created by vfcar on 23/07/2017.
 */
public class DtAuditoria implements DtWrapper {
    private long DT_RowId;
    private String nomeObjeto;
    private String idObjeto;
    private String operacao;
    private String dtHrEvento;
    private String usuario;

    public DtAuditoria() {
    }

    public DtAuditoria(Object[] rawObjects) {
        this.DT_RowId = rawObjects[0] == null ? 0 : ConvertUtils.convertTo(rawObjects[0], Long.class);
        this.nomeObjeto = rawObjects[1] == null ? "" : ConvertUtils.convertTo(rawObjects[1], String.class);
        this.idObjeto = rawObjects[2] == null ? "" : ConvertUtils.convertTo(rawObjects[2], String.class);
        this.operacao = rawObjects[3] == null ? "" : ConvertUtils.convertTo(rawObjects[3], String.class);
        this.dtHrEvento = rawObjects[4] == null ? "" : ConvertUtils.convertTo(rawObjects[4], String.class);
        this.usuario = rawObjects[5] == null ? "" : ConvertUtils.convertTo(rawObjects[5], String.class);
    }

    @Override
    public boolean isEmpty() {
        return DT_RowId == 0 &&
                (nomeObjeto == null || Objects.equals(nomeObjeto, "")) &&
                (idObjeto == null || Objects.equals(idObjeto, "")) &&
                (operacao == null || Objects.equals(operacao, "")) &&
                (dtHrEvento == null || Objects.equals(dtHrEvento, "")) &&
                (usuario == null || Objects.equals(usuario, ""));
    }

    public long getDT_RowId() {
        return DT_RowId;
    }

    public void setDT_RowId(long DT_RowId) {
        this.DT_RowId = DT_RowId;
    }

    public String getNomeObjeto() {
        return nomeObjeto;
    }

    public void setNomeObjeto(String nomeObjeto) {
        this.nomeObjeto = nomeObjeto;
    }

    public String getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(String idObjeto) {
        this.idObjeto = idObjeto;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getDtHrEvento() {
        return dtHrEvento;
    }

    public void setDtHrEvento(String dtHrEvento) {
        this.dtHrEvento = dtHrEvento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
