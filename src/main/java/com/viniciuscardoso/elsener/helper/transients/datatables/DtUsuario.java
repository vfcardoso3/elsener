package br.com.associaplus.associapp.helper.transients.datatables;

import com.viniciuscardoso.arch.vraptor.helper.datatables.DtWrapper;
import com.viniciuscardoso.arch.vraptor.utility.ConvertUtils;

import java.util.Objects;

/**
 * Created by vfcar on 23/07/2017.
 */
public class DtUsuario implements DtWrapper {

    private long rowid;
    private String status;
    private String nome;
    private String login;
    private String email;
    private String papel;

    public DtUsuario() {
    }

    public DtUsuario(Object[] rawObjects) {
        this.rowid = rawObjects[0] == null ? 0 : ConvertUtils.convertTo(rawObjects[0], Long.class);
        this.status = rawObjects[1] == null ? "" : ConvertUtils.convertTo(rawObjects[1], String.class);
        this.nome = rawObjects[2] == null ? "" : ConvertUtils.convertTo(rawObjects[2], String.class);
        this.login = rawObjects[3] == null ? "" : ConvertUtils.convertTo(rawObjects[3], String.class);
        this.email = rawObjects[4] == null ? "" : ConvertUtils.convertTo(rawObjects[4], String.class);
        this.papel = rawObjects[5] == null ? "" : ConvertUtils.convertTo(rawObjects[5], String.class);
    }

    @Override
    public boolean isEmpty() {
        return rowid == 0 &&
                (status == null || Objects.equals(status, "")) &&
                (nome == null || Objects.equals(nome, "")) &&
                (login == null || Objects.equals(login, "")) &&
                (email == null || Objects.equals(email, "")) &&
                (papel == null || Objects.equals(papel, ""));
    }

    public long getRowid() {
        return rowid;
    }

    public void setRowid(long rowid) {
        this.rowid = rowid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPapel() {
        return papel;
    }

    public void setPapel(String papel) {
        this.papel = papel;
    }
}
