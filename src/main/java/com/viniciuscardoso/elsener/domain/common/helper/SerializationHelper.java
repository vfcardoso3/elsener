package br.com.associaplus.associapp.domain.common.helper;

import com.google.gson.GsonBuilder;
import com.viniciuscardoso.arch.vraptor.infra.adapter.GsonLocalDateTimeTypeAdapter;
import com.viniciuscardoso.arch.vraptor.infra.adapter.GsonLocalDateTypeAdapter;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

/**
 * Created by vfcar on 23/07/2017.
 */
public class SerializationHelper {

    public static GsonBuilder getGsonBuilder() {
        return new GsonBuilder()
                .registerTypeAdapter(LocalDate.class, new GsonLocalDateTypeAdapter())
                .registerTypeAdapter(LocalDateTime.class, new GsonLocalDateTimeTypeAdapter());
    }
}