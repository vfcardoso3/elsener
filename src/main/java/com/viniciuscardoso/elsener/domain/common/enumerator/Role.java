package br.com.associaplus.associapp.domain.common.enumerator;

/**
 * Created by vfcar on 17/07/2017.
 */
public enum Role {
    SYSADMIN(0),
    OPERADOR(1);
    private int id;

    Role(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        switch (this.id) {
            case 0: return "SysAdmin";
            case 1: return "Operador";
            default: return "N/D";
        }
    }
}
