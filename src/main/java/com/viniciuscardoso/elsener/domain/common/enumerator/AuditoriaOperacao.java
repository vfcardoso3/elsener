package br.com.associaplus.associapp.domain.common.enumerator;

/**
 * Created by vfcar on 23/07/2017.
 */
public enum AuditoriaOperacao {
    INSERT(0),
    UPDATE(1),
    DELETE(2),
    LOGIN(3);
    private int id;

    AuditoriaOperacao(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        switch (this.id) {
            case 0: return "INSERT";
            case 1: return "UPDATE";
            case 2: return "DELETE";
            case 3: return "LOGIN";
            default: return "N/D";
        }
    }
}
