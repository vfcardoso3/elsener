package br.com.associaplus.associapp.domain.config;

import br.com.associaplus.associapp.domain.common.enumerator.Role;
import br.com.associaplus.associapp.helper.abstracts.AuditableEntity;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.viniciuscardoso.arch.vraptor.domain.interfaces.IActorAudited;
import com.viniciuscardoso.arch.vraptor.domain.interfaces.IAuditable;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

/**
 * Created by vfcar on 17/07/2017.
 */
@Entity
@Table(name = "USUARIO")
public class Usuario extends AuditableEntity<Usuario> implements IActorAudited, IAuditable<Usuario> {

    @Column(name = "NOME", nullable = false)
    private String nome;
    @Column(name = "EMAIL", nullable = false)
    private String email;
    @Column(name = "LOGIN", nullable = false, unique = true)
    private String login;
    @Column(name = "SENHA", updatable = false)
    private String senha;
    @Column(name = "ATIVO", nullable = false)
    private boolean ativo;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ROLE", nullable = false)
    private Role role;
    @Column(name = "FORGOT_PASSWD_KEY")
    private String forgotPasswordKey;

    //IAuditable
    @Column(name = "CREATED_AT")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentLocalDateTime")
    private LocalDateTime createdAt;
    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private Usuario createdBy;
    @Column(name = "CHANGED_AT")
    @Type(type = "org.joda.time.contrib.hibernate.PersistentLocalDateTime")
    private LocalDateTime changedAt;
    @ManyToOne
    @JoinColumn(name = "CHANGED_BY")
    private Usuario changedBy;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getForgotPasswordKey() {
        return forgotPasswordKey;
    }

    public void setForgotPasswordKey(String forgotPasswordKey) {
        this.forgotPasswordKey = forgotPasswordKey;
    }

    @Override
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Usuario getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(Usuario createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public LocalDateTime getChangedAt() {
        return changedAt;
    }

    @Override
    public void setChangedAt(LocalDateTime changedAt) {
        this.changedAt = changedAt;
    }

    @Override
    public Usuario getChangedBy() {
        return changedBy;
    }

    @Override
    public void setChangedBy(Usuario changedBy) {
        this.changedBy = changedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Usuario usuario = (Usuario) o;

        if (ativo != usuario.ativo) return false;
        if (!nome.equals(usuario.nome)) return false;
        if (!email.equals(usuario.email)) return false;
        if (!login.equals(usuario.login)) return false;
        return role == usuario.role;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + nome.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + (ativo ? 1 : 0);
        result = 31 * result + role.hashCode();
        return result;
    }

    @Override
    public JsonElement serialize(Usuario o, java.lang.reflect.Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", o.getId());
        jsonObject.addProperty("nome", o.getNome());
        jsonObject.addProperty("email", o.getEmail());
        jsonObject.addProperty("login", o.getLogin());
        jsonObject.addProperty("forgotPasswordKey", o.getForgotPasswordKey());
        jsonObject.addProperty("ativo", o.isAtivo());
        return jsonObject;
    }
}
