package br.com.associaplus.associapp.domain.config;

import br.com.associaplus.associapp.domain.common.enumerator.AuditoriaOperacao;
import br.com.associaplus.associapp.helper.abstracts.AbstractEntity;
import com.viniciuscardoso.arch.vraptor.domain.interfaces.IActorAudited;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

/**
 * Created by vfcar on 23/07/2017.
 */
@Entity
@Table(name = "AUDITORIA")
public class Auditoria extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "USUARIO_ID")
    private Usuario usuario;
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "OPERACAO", nullable = false)
    private AuditoriaOperacao operacao;
    @Column(name = "DT_HR_EVENTO", nullable = false)
    @Type(type = "org.joda.time.contrib.hibernate.PersistentLocalDateTime")
    private LocalDateTime dtHrEvento;
    @Column(name = "NOME_OBJETO", nullable = false)
    private String nomeObjeto;
    @Column(name = "ID_OBJETO", nullable = false)
    private long idObjeto;
    @Column(name = "JSON_OBJETO_NEW")
    @Type(type = "org.hibernate.type.TextType")
    @Lob
    private String jsonObjetoNovo;
    @Column(name = "JSON_OBJETO_OLD")
    @Type(type = "org.hibernate.type.TextType")
    @Lob
    private String jsonObjetoOld;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public AuditoriaOperacao getOperacao() {
        return operacao;
    }

    public void setOperacao(AuditoriaOperacao operacao) {
        this.operacao = operacao;
    }

    public LocalDateTime getDtHrEvento() {
        return dtHrEvento;
    }

    public void setDtHrEvento(LocalDateTime dtHrEvento) {
        this.dtHrEvento = dtHrEvento;
    }

    public String getNomeObjeto() {
        return nomeObjeto;
    }

    public void setNomeObjeto(String nomeObjeto) {
        this.nomeObjeto = nomeObjeto;
    }

    public long getIdObjeto() {
        return idObjeto;
    }

    public void setIdObjeto(long idObjeto) {
        this.idObjeto = idObjeto;
    }

    public String getJsonObjetoNovo() {
        return jsonObjetoNovo;
    }

    public void setJsonObjetoNovo(String jsonObjetoNovo) {
        this.jsonObjetoNovo = jsonObjetoNovo;
    }

    public String getJsonObjetoOld() {
        return jsonObjetoOld;
    }

    public void setJsonObjetoOld(String jsonObjetoOld) {
        this.jsonObjetoOld = jsonObjetoOld;
    }

    //Comportamento
    public void setActorAudited(IActorAudited actor) {
        this.usuario = (Usuario)actor;
    }
}