package br.com.associaplus.associapp.infra.converter;

import br.com.caelum.vraptor.Convert;
import br.com.caelum.vraptor.Converter;
import br.com.caelum.vraptor.core.Localization;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import java.util.Objects;
import java.util.ResourceBundle;

import static org.joda.time.format.DateTimeFormat.shortDate;

/**
 * Created by vfcar on 16/07/2017.
 */
@Convert(LocalDate.class)
public class CustomLocalDateConverter implements Converter<LocalDate> {

    private Localization localization;

    public CustomLocalDateConverter(Localization localization) {
        this.localization = localization;
    }

    @Override
    public LocalDate convert(String s, Class<? extends LocalDate> clazz, ResourceBundle resourceBundle) {

        if (s == null || Objects.equals(s, "")) return null;
        DateTime out = this.parse(s);

        if (out == null) return null;

        return out.toLocalDate();
    }

    private DateTime parse(String str) {
        // separate date from zone; you may need to adjust the pattern,
        // depending on what input formats you support
        DateTimeZone zone = DateTimeZone.forID("America/Sao_Paulo");

        // parsing in utc is safe, there are no gaps
        // parsing a LocalDate would also be appropriate,
        // but joda 1.6 doesn't support that
        DateTime utc = shortDate().withLocale(localization.getLocale()).withZone(DateTimeZone.UTC).parseDateTime(str);

        // false means don't be strict, joda will nudge the result forward by the
        // size of the gap.  The method is somewhat confusingly named, we're
        // actually going from UTC to local
        long millis = zone.convertLocalToUTC(utc.getMillis(), false);

        return new DateTime(millis, zone);
    }
}
