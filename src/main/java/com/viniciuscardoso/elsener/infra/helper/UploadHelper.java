package br.com.associaplus.associapp.infra.helper;

public final class UploadHelper {
    public final static long SIZE_5_MB = 5 * 1024 * 1024;
    public final static long SIZE_52_MB = 52 * 1024 * 1024;
    public final static String messagesBundle = "properties.messages";
}