package br.com.associaplus.associapp.infra.messaging;

import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.ioc.Component;

import java.util.Map;

/**
 * Created by vfcar on 25/07/2017.
 */
@Component
public class MessageRouter {

    private MessageSender messageSender;

    public MessageRouter(Environment environment) {
        this.messageSender = new MessageSender(environment);
    }

    public void sendEmail(String subject, Map<String, String> parameters, String... receiverList) {
        messageSender.sendEmail(subject, parameters, receiverList);
    }
}
