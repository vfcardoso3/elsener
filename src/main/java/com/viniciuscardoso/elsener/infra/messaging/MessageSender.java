package br.com.associaplus.associapp.infra.messaging;

import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.associaplus.associapp.infra.exception.AppException;
import br.com.caelum.vraptor.environment.Environment;
import com.sparkpost.Client;
import com.sparkpost.model.*;
import com.sparkpost.model.responses.Response;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.RestConnection;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.mail.EmailException;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static ch.lambdaj.Lambda.extract;
import static ch.lambdaj.Lambda.on;

/**
 * Created by vfcar on 25/07/2017.
 */
public class MessageSender {
    private final String apiKey;
    private final String apiEndPoint;
    private final Environment environment;

    public MessageSender(Environment environment) {
        this.apiKey = environment.get("smtp.apiKey");
        this.apiEndPoint = environment.get("smtp.apiEndPoint");
        this.environment = environment;
    }

    private Client getNewClientApiInstance() throws EmailException {
        return new Client(apiKey);
    }

    public void sendEmail(String subject, Map<String, String> parameters, String... receiverList) {
        try {
            Client client = this.getNewClientApiInstance();

            //Parâmetros pré-definidos
            parameters.put("nome_sistema", this.environment.get("config.nome_sistema"));
            parameters.put("nome_empresa", this.environment.get("config.empresa.nome"));
            parameters.put("endereco_empresa_linha1", this.environment.get("config.empresa.endereco_linha1"));
            parameters.put("endereco_empresa_linha2", this.environment.get("config.empresa.endereco_linha2"));
            parameters.put("site_empresa_with_protocol", this.environment.get("config.empresa.site_with_protocol"));
            parameters.put("site_empresa", this.environment.get("config.empresa.site"));
            parameters.put("ano", new LocalDate().toString("yyyy"));


            String msgFinal;
            String templateName = parameters.get("template_name") == null ? "generic" : parameters.get("template_name");
            String tmpl = new Scanner(MessageSender.class.getClassLoader()
                    .getResourceAsStream("mail_templates/" + templateName), "UTF-8").useDelimiter("\\Z").next();
            StrSubstitutor sub = new StrSubstitutor(parameters);
            msgFinal = sub.replace(tmpl);

            TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();

            OptionsAttributes options = new OptionsAttributes();
            options.setTransactional(true);
            transmission.setOptions(options);

            List<RecipientAttributes> recipientArray = new ArrayList<>();
            for (String recipient : receiverList) {
                RecipientAttributes ra = new RecipientAttributes();
                ra.setAddress(new AddressAttributes(recipient));
                recipientArray.add(ra);
            }
            transmission.setRecipientArray(recipientArray);

            TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
            contentAttributes.setFrom(new AddressAttributes(environment.get("smtp.default_from"),
                    environment.get("smtp.default_from_name"), environment.get("smtp.default_from")));
            contentAttributes.setSubject(subject);
            contentAttributes.setHtml(msgFinal);
            transmission.setContentAttributes(contentAttributes);

            RestConnection connection;
            connection = new RestConnection(client, apiEndPoint);
            Response response = ResourceTransmissions.create(connection, 0, transmission);

            System.out.println("Transmission Response: " + response);

        } catch (Exception e) {
            throw new AppException("Ocorreu um erro ao enviar o(s) e-mail(s). Causa: " + e.getMessage());
        }
    }

    public void sendEmail(String subject, Map<String, String> parameters, Usuario recipient) {
        this.sendEmail(subject, parameters, recipient.getEmail());
    }

    public void sendEmail(String subject, Map<String, String> parameters, List<Usuario> recipientList) {
        List<String> extract = extract(recipientList, on(Usuario.class).getEmail());
        String[] mailList = extract.toArray(new String[extract.size()]);
        this.sendEmail(subject, parameters, mailList);
    }
}
