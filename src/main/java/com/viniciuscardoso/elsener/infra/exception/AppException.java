package br.com.associaplus.associapp.infra.exception;

/**
 * Created by vfcar on 16/07/2017.
 */
public class AppException extends RuntimeException {

    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }
}