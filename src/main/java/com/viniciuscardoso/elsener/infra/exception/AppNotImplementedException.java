package br.com.associaplus.associapp.infra.exception;

/**
 * Created by vfcar on 16/07/2017.
 */
public class AppNotImplementedException extends RuntimeException {

    public AppNotImplementedException() {
        super();
    }

    public AppNotImplementedException(String message) {
        super(message);
    }

    public AppNotImplementedException(String message, Throwable cause) {
        super(message, cause);
    }
}