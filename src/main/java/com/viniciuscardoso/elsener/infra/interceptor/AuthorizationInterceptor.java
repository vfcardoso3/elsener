package br.com.associaplus.associapp.infra.interceptor;

import br.com.associaplus.associapp.app.controller.auth.PainelController;
import br.com.associaplus.associapp.domain.common.enumerator.Role;
import br.com.associaplus.associapp.infra.annotation.Permission;
import br.com.associaplus.associapp.infra.component.UserSession;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import com.viniciuscardoso.arch.vraptor.utility.ControllerUtils;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by vfcar on 17/07/2017.
 */
@Intercepts(after = LoginInterceptor.class)
public class AuthorizationInterceptor implements Interceptor {

    private Result result;
    private UserSession userSession;

    public AuthorizationInterceptor(Result result, UserSession userSession) {
        this.result = result;
        this.userSession = userSession;
    }

    @Override
    public void intercept(InterceptorStack is, ResourceMethod rm, Object o) throws InterceptionException {
        Permission methodPermission = rm.getMethod().getAnnotation(Permission.class);
        Permission controllerPermission = rm.getResource().getType().getAnnotation(Permission.class);

        if (this.hasAccess(methodPermission) && this.hasAccess(controllerPermission)) {
            is.next(rm, o);
        } else {
            ControllerUtils.defineErrorMessage(result, "Você não possui permissão para tal ação.");
            result.redirectTo(PainelController.class).index();
        }
    }

    @Override
    public boolean accepts(ResourceMethod rm) {
        return (rm.getMethod().isAnnotationPresent(Permission.class) || rm.getResource().getType().isAnnotationPresent(Permission.class));
    }

    private boolean hasAccess(Permission permission) {
        if (permission == null) {
            return true;
        }
        Collection<Role> perfilList = Arrays.asList(permission.value());
        return perfilList.contains(userSession.getUsuario().getRole());
    }
}
