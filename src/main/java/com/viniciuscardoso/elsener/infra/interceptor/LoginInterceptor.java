package br.com.associaplus.associapp.infra.interceptor;

import br.com.associaplus.associapp.app.controller.auth.AuthenticationController;
import br.com.associaplus.associapp.infra.component.UserSession;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.caelum.vraptor.view.Results;
import com.viniciuscardoso.arch.vraptor.infra.annotation.Public;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by vfcar on 17/07/2017.
 */
@Intercepts
public class LoginInterceptor implements Interceptor {
    private Result result;
    private UserSession userSession;
    private HttpServletRequest req;

    public LoginInterceptor(Result result, UserSession userSession, HttpServletRequest req) {
        this.result = result;
        this.userSession = userSession;
        this.req = req;
    }

    @Override
    public void intercept(InterceptorStack is, ResourceMethod rm, Object o) throws InterceptionException {
        if (userSession.isLogged()) {
            is.next(rm, o);
        } else {
            if (req.getMethod().toLowerCase().equals("post")) { //requisição ajax...
                result.use(Results.status()).badRequest("Sessão expirada! Pressione F5 para atualizar.");
            } else {
                String uri = req.getRequestURI().replaceAll(req.getContextPath(),"");
                String queryString = !uri.equals("/") ? uri : null;
                result.redirectTo(AuthenticationController.class).login(queryString);
            }
        }
    }

    @Override
    public boolean accepts(ResourceMethod rm) {
        return !(rm.getMethod().isAnnotationPresent(Public.class) || rm.getResource().getType().isAnnotationPresent(Public.class));
    }
}