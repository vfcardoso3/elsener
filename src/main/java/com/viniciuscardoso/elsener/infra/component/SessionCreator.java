package br.com.associaplus.associapp.infra.component;

import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import br.com.caelum.vraptor.ioc.RequestScoped;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by vfcar on 17/07/2017.
 */
@Component
@RequestScoped
public class SessionCreator implements ComponentFactory<Session> {

    private SessionFactory sessionFactory;
    private Session session;

    public SessionCreator(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @PostConstruct
    public void init() {
        if (this.session == null) {
            this.session = sessionFactory.openSession();
        }
    }

    @Override
    public Session getInstance() {
        return this.session;
    }

    @PreDestroy
    public void destroy() {
        if (this.session != null && this.session.isOpen()) {
            this.session.close();
        }
    }
}
