package br.com.associaplus.associapp.infra.component;

import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by vfcar on 17/07/2017.
 */
@Component
@ApplicationScoped
public class SessionFactoryCreator implements ComponentFactory<SessionFactory> {

    private SessionFactory sessionFactory;
    private Environment environment;

    public SessionFactoryCreator(Environment environment) {
        this.environment = environment;
    }

    @PostConstruct
    public void init() {
        this.sessionFactory = new Configuration().configure(environment.getResource("/hibernate.cfg.xml")).buildSessionFactory();
    }

    @Override
    public SessionFactory getInstance() {
        return this.sessionFactory;
    }

    @PreDestroy
    public void destroy() {
        this.sessionFactory.close();
    }
}
