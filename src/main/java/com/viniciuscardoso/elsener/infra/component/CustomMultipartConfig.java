package br.com.associaplus.associapp.infra.component;

import br.com.associaplus.associapp.infra.helper.UploadHelper;
import br.com.caelum.vraptor.interceptor.multipart.DefaultMultipartConfig;
import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

/**
 * Project: daimo
 * User: Vinícius
 * Date: 17/03/2015
 * Time: 13:20
 */
@Component
@ApplicationScoped
public class CustomMultipartConfig extends DefaultMultipartConfig {

    public long getSizeLimit() {
        return UploadHelper.SIZE_52_MB;
    }
}
