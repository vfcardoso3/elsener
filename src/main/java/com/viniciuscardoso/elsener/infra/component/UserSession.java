package br.com.associaplus.associapp.infra.component;

import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * Created by vfcar on 17/07/2017.
 */
@Component
@SessionScoped
public class UserSession implements Serializable {

    private Usuario usuario;
    private HttpSession httpSession;

    public UserSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    //<editor-fold desc="[Login methods]">
    public void setLoggedUsuario(Usuario u) {
        this.usuario = u;
    }

    public boolean isLogged() {
        return usuario != null;
    }

    public void logout() {
        this.usuario = null;
        this.httpSession.invalidate();
    }
    //</editor-fold>

    //<editor-fold desc="[Getters e Setters]">
    public Usuario getUsuario() {
        return usuario;
    }
    //</editor-fold>
}
