package br.com.associaplus.associapp.app.controller.config;

import br.com.associaplus.associapp.app.dao.config.UsuarioDao;
import br.com.associaplus.associapp.domain.common.enumerator.Role;
import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.associaplus.associapp.helper.abstracts.AuditableController;
import br.com.associaplus.associapp.helper.messaging.MessageBuilder;
import br.com.associaplus.associapp.helper.transients.datatables.DtUsuario;
import br.com.associaplus.associapp.helper.utility.ExportHelper;
import br.com.associaplus.associapp.infra.component.UserSession;
import br.com.associaplus.associapp.infra.exception.AppException;
import br.com.associaplus.associapp.infra.messaging.MessageRouter;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.environment.Environment;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.view.Results;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTables;
import com.viniciuscardoso.arch.vraptor.utility.ControllerUtils;
import com.viniciuscardoso.arch.vraptor.utility.EncryptionUtils;
import com.viniciuscardoso.arch.vraptor.utility.JsonUtils;

import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vfcar on 23/07/2017.
 */
@Resource
public class UsuarioController extends AuditableController<UsuarioDao, Usuario> {

    private Result result;
    private UsuarioDao usuarioDao;
    private UserSession userSession;
    private MessageRouter messageRouter;
    private Environment environment;
    private HttpServletResponse response;

    public UsuarioController(Result result, UsuarioDao usuarioDao, UserSession userSession, Environment environment,
                             MessageRouter messageRouter, HttpServletResponse response) {
        super(result, usuarioDao);
        this.result = result;
        this.usuarioDao = usuarioDao;
        this.userSession = userSession;
        this.messageRouter = messageRouter;
        this.environment = environment;
        this.response = response;
    }

    @Get("/painel/usuario/list")
    public void list() {
    }

    @Get("/painel/usuario/create")
    public void create() {
        result.include("papelList", Role.values());
    }

    @Post("/painel/usuario/add")
    public void add(Usuario usuario, boolean enviarEmail, boolean enviarCopia, String emailCopia) {
        try {
            String senhaPlain = usuario.getSenha();
            usuarioDao.save(usuario, userSession.getUsuario());
            if (enviarEmail) {
                String titulo = "Novo usuário cadastrado";
                String msg = MessageBuilder.welcomeNewUser(usuario.getLogin(), senhaPlain, this.environment.get("config.app_address"),
                        this.environment.get("config.nome_sistema"));
                Map<String, String> parameters = new HashMap<>();
                parameters.put("titulo_mensagem", titulo);
                parameters.put("mensagem", msg);

                if (enviarCopia) {
                    emailCopia = emailCopia + "," + usuario.getEmail();
                    String[] emails = emailCopia.split(",");
                    messageRouter.sendEmail(titulo, parameters, emails);
                } else {
                    messageRouter.sendEmail(titulo, parameters, usuario.getEmail());
                }
            }
            ControllerUtils.defineSuccessMessage(result, "Usuário adicionado");
            result.redirectTo(this).list();
        } catch (Exception e) {
            ControllerUtils.defineErrorMessage(result, e);
            result.include("usuario", usuario);
            result.redirectTo(this).create();
        }
    }

    @Get("/painel/usuario/edit/{id}")
    public void edit(Long id) {
        result.include("papelList", Role.values());
        super.edit(id, "usuario");
    }

    @Post("/painel/usuario/update")
    public void update(Usuario usuario) {
        try {
            usuarioDao.update(usuario, userSession.getUsuario());

            ControllerUtils.defineSuccessMessage(result, "Usuário atualizado");
            result.redirectTo(this).list();
        } catch (Exception e) {
            ControllerUtils.defineErrorMessage(result, e);
            result.redirectTo(this).edit(usuario.getId());
        }
    }

    @Get("/painel/usuario/remove/{id}")
    public void remove(Long id) {
        try {
            Usuario u = usuarioDao.getById(id);
            if (u != null) {
                usuarioDao.remove(u, userSession.getUsuario());
                result.use(Results.status()).ok();
            } else {
                throw new AppException("Usuário não existe.");
            }
        } catch (Exception e) {
            JsonUtils.setErrorJsonResult(result, e);
        }
    }

    @Post("/painel/usuario/list.json")
    public void listDtJson(Integer draw, DtUsuario dtUsuario, int start, int length, @Named("order[0][column]") int orderColumn,
                           @Named("order[0][dir]") String orderDirection) {
        try {
            DataTables<DtUsuario> dataTables = usuarioDao.listJson(dtUsuario, start, length, orderColumn, orderDirection);
            dataTables.setDraw(draw);
            result.use(Results.json()).withoutRoot().from(dataTables).include("data").serialize();
        } catch (Exception e) {
            JsonUtils.setErrorJsonResult(result, e);
        }
    }

    @Get("/painel/usuario/list/export")
    public Download listExport(DtUsuario dtUsuario, @Named("order[0][column]") int orderColumn,
                               @Named("order[0][dir]") String orderDirection) throws NoSuchMethodException, InstantiationException,
            IllegalAccessException, InvocationTargetException, IOException {

        DataTables<DtUsuario> dataTables = usuarioDao.listJson(dtUsuario, 0, -1, orderColumn, orderDirection);
        Download download = new ExportHelper<DtUsuario>(){}.generateDownload(dataTables);

        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        return download;
    }

    @Post("/painel/usuario/view.json")
    public void viewJson(Long id) {
        try {
            Usuario u = usuarioDao.getById(id);
            result.use(Results.json()).withoutRoot().from(u)
                    .exclude("senha")
                    .serialize();
        } catch (Exception e) {
            JsonUtils.setErrorJsonResult(result, e);
        }
    }

    @Get("/painel/usuario/resend-welcome-email/{id}")
    public void resendWelcomeEmail(Long id) {
        try {
            Usuario u = usuarioDao.getById(id);
            if (u != null) {
                String newPasswd = EncryptionUtils.getRandomString(12, true, true, false);
                u.setSenha(newPasswd);
                usuarioDao.changePassword(u, userSession.getUsuario());

                String titulo = "Reenvio de credenciais de usuário";
                String msg = MessageBuilder.welcomeNewUser(u.getLogin(), newPasswd, this.environment.get("config.app_address"),
                        this.environment.get("config.nome_sistema"));
                Map<String, String> parameters = new HashMap<>();
                parameters.put("titulo_mensagem", titulo);
                parameters.put("mensagem", msg);

                messageRouter.sendEmail(titulo, parameters, u.getEmail());

                result.use(Results.status()).ok();
            } else {
                throw new AppException("Usuário não existe.");
            }
        } catch (Exception e) {
            JsonUtils.setErrorJsonResult(result, e);
        }
    }

    @Post("/painel/usuario/change-password")
    public void changePassword(Usuario usuario) throws Exception {
        try {
            usuarioDao.changePassword(usuario, userSession.getUsuario());
            ControllerUtils.defineSuccessMessage(result, "Senha alterada");
        } catch (Exception e) {
            ControllerUtils.defineErrorMessage(result, e);
        } finally {
            result.redirectTo(this).list();
        }
    }
}