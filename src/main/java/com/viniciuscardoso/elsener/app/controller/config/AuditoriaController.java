package br.com.associaplus.associapp.app.controller.config;

import br.com.associaplus.associapp.app.dao.config.AuditoriaDao;
import br.com.associaplus.associapp.domain.config.Auditoria;
import br.com.associaplus.associapp.helper.transients.datatables.DtAuditoria;
import br.com.associaplus.associapp.helper.utility.ExportHelper;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.download.Download;
import br.com.caelum.vraptor.view.Results;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTables;
import com.viniciuscardoso.arch.vraptor.utility.ControllerUtils;
import com.viniciuscardoso.arch.vraptor.utility.JsonUtils;
import org.joda.time.LocalDate;

import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by vfcar on 23/07/2017.
 */

@Resource
public class AuditoriaController  {

    private Result result;
    private AuditoriaDao auditoriaDao;
    private HttpServletResponse response;

    public AuditoriaController(Result result, AuditoriaDao auditoriaDao, HttpServletResponse response) {
        this.result = result;
        this.auditoriaDao = auditoriaDao;
        this.response = response;
    }

    @Get("/painel/auditoria/list")
    public void list() {
        ControllerUtils.defineTodayMinusWeeks(result, 1, "dd/MM/yyyy");
    }

    @Post("/painel/auditoria/list.json")
    public void listDtJson(DtAuditoria dtAuditoria, LocalDate dtInicio, LocalDate dtFim, Integer draw, int start, int length,
                           @Named("order[0][column]") int orderColumn, @Named("order[0][dir]") String orderDirection) {
        try {
            DataTables<DtAuditoria> dataTables;
            dataTables = auditoriaDao.listJson(dtAuditoria, dtInicio, dtFim, start, length, orderColumn, orderDirection);
            dataTables.setDraw(draw);
            result.use(Results.json()).withoutRoot().from(dataTables).include("data").serialize();
        } catch (Exception e) {
            JsonUtils.setErrorJsonResult(result, e);
        }
    }

    @Get("/painel/auditoria/list/export")
    public Download listExport(DtAuditoria dtAuditoria, LocalDate dtInicio, LocalDate dtFim, @Named("order[0][column]") int orderColumn,
                               @Named("order[0][dir]") String orderDirection)
            throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {

        DataTables<DtAuditoria> dataTables = auditoriaDao.listJson(dtAuditoria, dtInicio, dtFim, 0, -1, orderColumn, orderDirection);
        Download download = new ExportHelper<DtAuditoria>(){}.generateDownload(dataTables);

        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        return download;
    }

    @Post("/painel/auditoria/view.partial")
    public void view(Long id) {
        try {
            Auditoria a = auditoriaDao.getById(id);
            result.include("auditoria", a);
        } catch (Exception e) {
            ControllerUtils.defineErrorMessage(result, e);
        }
    }
}
