package br.com.associaplus.associapp.app.controller.front;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Head;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import com.viniciuscardoso.arch.vraptor.infra.annotation.Public;

/**
 * Created by vfcar on 16/07/2017.
 */
@Resource
public class FrontIndexController {

    private Result result;

    public FrontIndexController(Result result) {
        this.result = result;
    }

    @Public
    @Head
    @Get("/")
    public void index() {
        result.include("payload", "Front-end working!");
    }
}
