package br.com.associaplus.associapp.app.controller.auth;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

/**
 * Created by vfcar on 16/07/2017.
 */
@Resource
public class PainelController {

    private Result result;

    public PainelController(Result result) {
        this.result = result;
    }

    @Get("/painel")
    public void index() {
        result.include("payload", "It works!");
    }
}
