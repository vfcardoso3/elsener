package br.com.associaplus.associapp.app.controller.auth;

import br.com.associaplus.associapp.app.dao.auth.AuthenticationDao;
import br.com.associaplus.associapp.app.dao.config.AuditoriaDao;
import br.com.associaplus.associapp.domain.common.enumerator.AuditoriaOperacao;
import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.associaplus.associapp.helper.messaging.MessageBuilder;
import br.com.associaplus.associapp.infra.component.UserSession;
import br.com.associaplus.associapp.infra.messaging.MessageRouter;
import br.com.caelum.vraptor.*;
import br.com.caelum.vraptor.environment.Environment;
import com.viniciuscardoso.arch.vraptor.infra.annotation.Internal;
import com.viniciuscardoso.arch.vraptor.infra.annotation.Public;
import com.viniciuscardoso.arch.vraptor.utility.ControllerUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Panda Bond on 12/06/2015.
 */
@Resource
public class AuthenticationController {

    private UserSession userSession;
    private Result result;
    private HttpServletRequest request;
    private AuthenticationDao authenticationDao;
    private AuditoriaDao auditoriaDao;
    private Environment environment;
    private MessageRouter messageRouter;

    public AuthenticationController(UserSession userSession, Result result, HttpServletRequest request, AuthenticationDao authenticationDao,
                                    AuditoriaDao auditoriaDao, Environment environment, MessageRouter messageRouter) {
        this.userSession = userSession;
        this.result = result;
        this.request = request;
        this.authenticationDao = authenticationDao;
        this.auditoriaDao = auditoriaDao;
        this.environment = environment;
        this.messageRouter = messageRouter;
    }

    @Public
    @Get("/painel/login?{query}")
    @Head
    public void login(String query) {
    }

    @Public
    @Post("/painel/authenticate")
    public void authenticate(Usuario usuario, String queryString) throws Exception {
        String ip = InetAddress.getLocalHost().getHostAddress();

        if (usuario.getLogin() != null && usuario.getSenha() != null) {
            Usuario u = authenticationDao.authenticate(usuario.getLogin(), usuario.getSenha());

            if (u != null) {
                if(userSession.isLogged()) userSession.logout();

                userSession.setLoggedUsuario(u);
                request.getSession().setAttribute("userName", u.getLogin());

                auditoriaDao.createAuditLog(u, AuditoriaOperacao.LOGIN, "LoginPermitido", -2L, "{}",
                        "{\"login\":\"" + usuario.getLogin() + "\",\"ip\":\"" + ip + "\"}");

                if (queryString != null && !queryString.isEmpty() && queryString.startsWith("/")) {
                    result.redirectTo(queryString);
                } else {
                    result.redirectTo(PainelController.class).index();
                }
            } else {
                defineLoginErrorMessage(queryString, usuario.getLogin(), ip);
            }
        } else {
            defineLoginErrorMessage(queryString, usuario.getLogin(), ip);
        }
    }

    @Public
    @Get("/painel/forgot-password")
    public void forgotPassword() {
    }

    @Public
    @Post("/painel/forgot-password")
    public void forgotPasswordPost(String email) throws Exception {
        try {
            email = (email != null ? email.trim() : "");
            Usuario u = authenticationDao.checkForgottenPassword(email);
            if (u != null) {
                String fgtPasswdKey = authenticationDao.generateForgotPasswordKey(u);

                String hyperlink = this.environment.get("config.app_address") + "/painel/validate-forgotten-password?" + fgtPasswdKey;
                String titulo = "Esqueci minha senha";
                String mailMessage = MessageBuilder.userForgottenPassword(hyperlink, this.environment.get("config.nome_sistema"));

                Map<String, String> parameters = new HashMap<>();
                parameters.put("titulo_mensagem", titulo);
                parameters.put("mensagem", mailMessage);

                messageRouter.sendEmail(titulo, parameters, u.getEmail());

                result.include("message", "Solicitação efetuada com sucesso! Um e-mail foi enviado com as instruções.");
                result.include("clazz", "alert-success");
            } else {
                result.include("message", "Não existe usuário correspondente ao e-mail informado.");
                result.include("clazz", "alert-danger");
            }
        } catch (Exception e) {
            result.include("message", e.getMessage());
            result.include("clazz", "alert-danger");
        } finally {
            result.redirectTo(this).forgotPassword();
        }
    }

    @Public
    @Get("/painel/validate-forgotten-password?{fgtPasswordKey}")
    public void validateForgottenPassword(String fgtPasswordKey) {
    }

    @Public
    @Get("/painel/validate-password-success")
    public void validatePasswordSuccess() {
    }

    @Public
    @Post("/painel/validate-forgotten-password")
    public void validateForgottenPasswordPost(Usuario usuario) throws Exception {
        try {
            Usuario u = authenticationDao.validateForgotPasswordKey(usuario.getForgotPasswordKey());
            if (u != null) {
                authenticationDao.updateForgottenPassword(usuario.getForgotPasswordKey(), usuario.getSenha());
                result.redirectTo(this).validatePasswordSuccess();
            } else {
                result.include("message", "Não existe usuário correspondente à chave informada.");
                result.redirectTo(this).forgotPassword();
                result.include("clazz", "alert-danger");
            }
        } catch (Exception e) {
            result.include("message", e.getMessage());
            result.include("clazz", "alert-danger");
            result.redirectTo(this).validateForgottenPassword(usuario.getForgotPasswordKey());
        }
    }

    @Internal
    @Post("/painel/change-logged-user-password")
    public void changeLoggedUserPassword(Usuario usuario, String currPassword, String newPassword) throws Exception {
        try {
            authenticationDao.changeLoggedUserPassword(usuario, currPassword, newPassword);
            ControllerUtils.defineSuccessMessage(result, "Senha alterada", "Reinicie a sessão para que as alterações tenham efeito.");
        } catch (Exception e) {
            ControllerUtils.defineErrorMessage(result, e.toString());
        }
        result.redirectTo(PainelController.class).index();
    }

    @Internal
    @Get("/painel/logout")
    public void logout() {
        userSession.logout();
        request.getSession().setAttribute("userName", "");
        result.redirectTo(this).login(null);
    }

    private void defineLoginErrorMessage(String queryString, String usuarioLogin, String ip) {
        auditoriaDao.createAuditLog(null, AuditoriaOperacao.LOGIN, "LoginNegado", -2L, "{}", "{\"login\":\"" + usuarioLogin + "\",\"ip\":\"" + ip + "\"}");

        result.include("error", "Usuário ou senha incorretos!");
        result.include("usuarioLogin", usuarioLogin);
        result.redirectTo(this).login(queryString);
    }
}
