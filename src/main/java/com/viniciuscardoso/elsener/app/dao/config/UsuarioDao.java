package br.com.associaplus.associapp.app.dao.config;

import br.com.associaplus.associapp.domain.common.enumerator.Role;
import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.associaplus.associapp.helper.abstracts.AuditableDao;
import br.com.associaplus.associapp.helper.interfaces.InactivatableDao;
import br.com.associaplus.associapp.helper.transients.datatables.DtUsuario;
import br.com.caelum.vraptor.ioc.Component;
import ch.lambdaj.Lambda;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTables;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTablesHelper;
import com.viniciuscardoso.arch.vraptor.utility.EncryptionUtils;
import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.joda.time.LocalDateTime;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;

/**
 * Created by vfcar on 23/07/2017.
 */
@Component
public class UsuarioDao extends AuditableDao<Usuario, Usuario> implements InactivatableDao<Usuario> {

    public UsuarioDao(Session session) {
        super(session, UsuarioDao.class);
    }

    @Override
    public void save(Usuario usuario, Usuario actor) {
        try {
            String criptoSenha = EncryptionUtils.getHash(usuario.getSenha(), "SHA-1");
            usuario.setSenha(criptoSenha);
            super.save(usuario, actor);
            super.getLogger().info("Usuário adicionado com sucesso no id = " + String.valueOf(usuario.getId()));
        } catch (Exception e) {
            super.getLogger().error(e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public DataTables<DtUsuario> listJson(DtUsuario dtUsuario, int start, int length, int orderColumn, String orderDirection)
            throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        StringBuilder consulta = new StringBuilder();

        consulta.append("select  ")
                .append("   u.ID as ROWID, ")
                .append("   case when u.ATIVO = true then 'ATIVO' else 'DESATIVADO' end as STATUS, ")
                .append("   u.NOME as NOME, ")
                .append("   u.LOGIN as LOGIN, ")
                .append("   u.EMAIL as EMAIL, ")
                .append("   case ");
        for(Role p : Role.values()) {
            consulta.append("   when u.ROLE = ").append(p.getId()).append(" then '").append(p.toString()).append("' ");
        }
        consulta.append("   end as PAPEL ")
                .append("from ")
                .append("	USUARIO u ");

        consulta.append("where 1=1 ");

        if (dtUsuario.getRowid() != 0)
            consulta.append("   and u.ID = :rowid ");
        if (dtUsuario.getStatus() != null && !Objects.equals(dtUsuario.getStatus(), ""))
            consulta.append("   and (select case when u1.ATIVO = true then 'ATIVO' else 'DESATIVADO' end from USUARIO u1 where u1.ID = u.ID) like :status ");
        if (dtUsuario.getNome() != null && !Objects.equals(dtUsuario.getNome(), ""))
            consulta.append("   and u.NOME like :nome ");
        if (dtUsuario.getLogin() != null && !Objects.equals(dtUsuario.getLogin(), ""))
            consulta.append("   and u.LOGIN like :login ");
        if (dtUsuario.getEmail() != null && !Objects.equals(dtUsuario.getEmail(), ""))
            consulta.append("   and u.EMAIL like :email ");
        if (dtUsuario.getPapel() != null && !Objects.equals(dtUsuario.getPapel(), "")) {
            consulta.append("   and (select case ");
            for(Role p : Role.values()) {
                consulta.append("   when u1.ROLE = ").append(p.getId()).append(" then '").append(p.toString()).append("' ");
            }
            consulta.append("   end from USUARIO u1 where u1.ID = u.ID) like :papel ");
        }

        consulta.append("order by ").append(orderColumn + 1).append(" ").append(orderDirection);

        Query q = super.getSession().createSQLQuery(consulta.toString());

        if (dtUsuario.getRowid() != 0)
            q.setParameter("rowid", dtUsuario.getRowid());
        if (dtUsuario.getStatus() != null && !Objects.equals(dtUsuario.getStatus(), ""))
            q.setParameter("status", '%' + dtUsuario.getStatus() + '%');
        if (dtUsuario.getNome() != null && !Objects.equals(dtUsuario.getNome(), ""))
            q.setParameter("nome", '%' + dtUsuario.getNome() + '%');
        if (dtUsuario.getLogin() != null && !Objects.equals(dtUsuario.getLogin(), ""))
            q.setParameter("login", '%' + dtUsuario.getLogin() + '%');
        if (dtUsuario.getEmail() != null && !Objects.equals(dtUsuario.getEmail(), ""))
            q.setParameter("email", '%' + dtUsuario.getEmail() + '%');
        if (dtUsuario.getPapel() != null && !Objects.equals(dtUsuario.getPapel(), ""))
            q.setParameter("papel", '%' + dtUsuario.getPapel() + '%');

        q.setCacheMode(CacheMode.REFRESH);

        return new DataTablesHelper<>(DtUsuario.class).getDatatablesFromRawObjectArray(q.list(), dtUsuario.isEmpty(),
                start, length, this.countAll());
    }



    public void changePassword(Usuario usuario, Usuario actor) {
        String cryptoPassword = EncryptionUtils.getHash(usuario.getSenha(), "SHA-1");
        Transaction t = super.getSession().getTransaction();
        try {
            StringBuilder consulta = new StringBuilder();
            consulta.append("update USUARIO u set ")
                    .append("u.SENHA = :cryptoSenha, ")
                    .append("u.CHANGED_AT = :dtAlteracao, ")
                    .append("u.CHANGED_BY = :actor ")
                    .append("where u.ID = :id ");

            Query q = super.getSession().createSQLQuery(consulta.toString());
            q.setParameter("cryptoSenha", cryptoPassword);
            q.setParameter("dtAlteracao", new LocalDateTime().toString("yyyy-MM-dd HH:mm:ss"));
            q.setParameter("actor", actor);
            q.setParameter("id", usuario.getId());
            t.begin();
            q.executeUpdate();
            t.commit();
            super.getLogger().info("Senha do usuário alterada com sucesso no id = " + String.valueOf(usuario.getId()));
        } catch (Exception e) {
            if(!t.wasCommitted()) t.rollback();
            super.getLogger().error(e);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Usuario> listActiveUserByPapel(Role role) {
        String c = "from Usuario u where u.papel = :papel order by u.nome";
        Query q = super.getSession().createQuery(c);
        q.setParameter("papel", role);
        return q.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Usuario> listAtivosSortedBy(String... fields) {
        String q = "from Usuario t where t.ativo = true ";
        if(fields.length > 0) {
            q = q + " order by " + Lambda.join(fields, ", ");
        }
        return super.getSession().createQuery(q).list();
    }
}
