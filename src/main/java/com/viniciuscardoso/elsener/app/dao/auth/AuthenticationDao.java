package br.com.associaplus.associapp.app.dao.auth;

import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.caelum.vraptor.ioc.Component;
import com.viniciuscardoso.arch.vraptor.utility.EncryptionUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by vfcar on 17/07/2017.
 */
@Component
public class AuthenticationDao {
    
    private final Logger logger;
    private Session session;

    public AuthenticationDao(Session session) {
        this.session = session;
        this.logger = Logger.getLogger(AuthenticationDao.class);
    }

    public Usuario authenticate(String login, String password) {
        try {
            Query query = this.session.createQuery("from Usuario u where u.login = :login and u.senha = :senha and u.ativo = true");
            query.setParameter("login", login);
            query.setParameter("senha", EncryptionUtils.getHash(password, "SHA-1"));
            Usuario u = (Usuario) query.uniqueResult();
            if (u != null) {
                this.logger.info("Usuário [login: " + u.getLogin() + ", id: " + String.valueOf(u.getId()) + "] autenticado e logado no sistema.");
            } else {
                this.logger.info("Falha na autenticação de usuário no sistema [login tentado: " + login + "].");
            }
            return u;
        } catch (Exception e) {
            this.logger.error(e);
            throw e;
        }
    }

    public Usuario checkForgottenPassword(String email) {
        try {
            Query query = this.session.createQuery("from Usuario u where u.email = :email and u.ativo = true");
            query.setParameter("email", email);
            return (Usuario) query.uniqueResult();
        } catch (Exception e) {
            this.logger.error(e);
            throw e;
        }
    }

    public String generateForgotPasswordKey(Usuario usuario) {
        Transaction tx = null;
        try {
            String fgtPasswdKey = EncryptionUtils.getRandomString(128, true, true, false);
            usuario.setForgotPasswordKey(fgtPasswdKey);
            tx = this.session.beginTransaction();
            this.session.update(usuario);
            tx.commit();
            return fgtPasswdKey;
        } catch (Exception e) {
            if (tx != null && !tx.wasRolledBack()) tx.rollback();
            this.logger.error(e);
            throw e;
        }
    }

    public Usuario validateForgotPasswordKey(String fgtPasswdKey) {
        try {
            Query query = this.session.createQuery("from Usuario u where u.forgotPasswordKey = :forgotPasswordKey and u.ativo = true");
            query.setParameter("forgotPasswordKey", fgtPasswdKey);
            return (Usuario) query.uniqueResult();
        } catch (Exception e) {
            this.logger.error(e);
            throw e;
        }
    }

    public void updateForgottenPassword(String fgtPasswordKey, String password) {
        String cryptoPassword = EncryptionUtils.getHash(password, "SHA-1");
        Transaction t = this.session.getTransaction();
        try {
            Query q = this.session.createSQLQuery("update USUARIO u set u.SENHA = :cryptoSenha, u.FORGOT_PASSWD_KEY = null where u.FORGOT_PASSWD_KEY = :forgotPasswordKey");
            q.setParameter("cryptoSenha", cryptoPassword);
            q.setParameter("forgotPasswordKey", fgtPasswordKey);
            t.begin();
            q.executeUpdate();
            t.commit();
            this.logger.info("Senha do usuário alterada com sucesso para a chave = " + fgtPasswordKey);
        } catch (Exception e) {
            if(!t.wasCommitted()) t.rollback();
            this.logger.error(e);
            throw e;
        }
    }

    public void changeLoggedUserPassword(Usuario usuario, String currentPassword, String newPassword) {
        String cryptoAtualSenha = EncryptionUtils.getHash(currentPassword, "SHA-1");
        String cryptoNovaSenha = EncryptionUtils.getHash(newPassword, "SHA-1");
        Transaction t = this.session.getTransaction();
        try {
            Query q = this.session.createSQLQuery("update USUARIO u set u.SENHA = :cryptoNovaSenha where u.SENHA = :cryptoAtualSenha and u.ID = :id");
            q.setParameter("cryptoNovaSenha", cryptoNovaSenha);
            q.setParameter("cryptoAtualSenha", cryptoAtualSenha);
            q.setParameter("id", usuario.getId());
            t.begin();
            int executeUpdate = q.executeUpdate();
            t.commit();
            if (executeUpdate == 0) {
                throw new HibernateException("Senha atual inválida!");
            }
        } catch (Exception e) {
            if(!t.wasCommitted()) t.rollback();
            this.logger.error(e);
            throw e;
        }
    }
}
