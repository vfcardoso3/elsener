package br.com.associaplus.associapp.app.dao.config;

import br.com.associaplus.associapp.domain.common.enumerator.AuditoriaOperacao;
import br.com.associaplus.associapp.domain.config.Auditoria;
import br.com.associaplus.associapp.domain.config.Usuario;
import br.com.associaplus.associapp.helper.transients.datatables.DtAuditoria;
import br.com.caelum.vraptor.ioc.Component;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTables;
import com.viniciuscardoso.arch.vraptor.helper.datatables.DataTablesHelper;
import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

/**
 * Created by vfcar on 23/07/2017.
 */
@Component
public class AuditoriaDao  {

    private Session session;

    public AuditoriaDao(Session session) {
        this.session = session;
    }

    public Auditoria getById(Long id) {
        Auditoria a = (Auditoria) this.session.get(Auditoria.class, id);
        return a;
    }

    public void createAuditLog(Usuario creator, AuditoriaOperacao operacao, String nomeObjeto, Long id, String jsonOld, String jsonNew) {
        //Auditoria
        Auditoria auditoria = new Auditoria();
        auditoria.setActorAudited(creator);
        auditoria.setOperacao(operacao);
        auditoria.setDtHrEvento(new LocalDateTime());
        auditoria.setNomeObjeto(nomeObjeto);
        auditoria.setIdObjeto(id);
        auditoria.setJsonObjetoOld(jsonOld);
        auditoria.setJsonObjetoNovo(jsonNew);

        this.session.getTransaction().begin();
        this.session.save(auditoria);
        this.session.getTransaction().commit();
    }

    @SuppressWarnings("unchecked")
    public DataTables<DtAuditoria> listJson(DtAuditoria dtAuditoria, LocalDate dtInicio, LocalDate dtFim, int start, int length,
                                            int orderColumn, String orderDirection)
            throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        StringBuilder consulta = new StringBuilder();

        consulta.append("SELECT ")
                .append("	a.ID as ROWID, ")
                .append("	a.NOME_OBJETO as NOME_OBJETO, ")
                .append("	a.ID_OBJETO as ID_OBJETO, ")
                .append("   case ");
        for (AuditoriaOperacao o : AuditoriaOperacao.values()) {
            consulta.append("when a.OPERACAO = ").append(o.getId()).append(" then '").append(o.toString().toUpperCase()).append("' ");
        }
        consulta.append("   end as OPERACAO, ")
                .append("	DATE_FORMAT(a.DT_HR_EVENTO, '%Y/%m/%d %H:%i:%s') as DT_HR_EVENTO, ")
                .append("	u.NOME ")
                .append("FROM ")
                .append("	AUDITORIA a ")
                .append("	left join USUARIO u on u.ID = a.USUARIO_ID ")
                .append("WHERE 1=1 ")
                .append("	and a.DT_HR_EVENTO between :dtInicio and :dtFim ");

        String consultaAll = consulta.toString();

        if (dtAuditoria.getDT_RowId() != 0)
            consulta.append("   and a.ID = :rowid ");
        if (dtAuditoria.getNomeObjeto() != null && !Objects.equals(dtAuditoria.getNomeObjeto(), ""))
            consulta.append("   and a.NOME_OBJETO like :nomeObjeto ");
        if (dtAuditoria.getIdObjeto() != null && !Objects.equals(dtAuditoria.getIdObjeto(), ""))
            consulta.append("   and a.ID_OBJETO like :idObjeto ");
        if (dtAuditoria.getOperacao() != null && !Objects.equals(dtAuditoria.getOperacao(), "")) {
            consulta.append("   and (select case ");
            for (AuditoriaOperacao o : AuditoriaOperacao.values()) {
                consulta.append("when a1.OPERACAO = ").append(o.getId()).append(" then '").append(o.toString().toUpperCase()).append("' ");
            }
            consulta.append("	end from AUDITORIA a1 where a1.ID = a.ID) like :operacao ");
        }
        if (dtAuditoria.getDtHrEvento() != null && !Objects.equals(dtAuditoria.getDtHrEvento(), ""))
            consulta.append("   and DATE_FORMAT(a.DT_HR_EVENTO, '%d/%m/%Y %H:%i:%s') like :dtHrEvento ");
        if (dtAuditoria.getUsuario() != null && !Objects.equals(dtAuditoria.getUsuario(), ""))
            consulta.append("   and u.NOME like :usuario ");

        consulta.append("order by ").append(orderColumn + 2).append(" ").append(orderDirection);

        Query q = this.session.createSQLQuery(consulta.toString());
        q.setParameter("dtInicio", dtInicio.toDateTimeAtStartOfDay().toString("yyyy-MM-dd HH:mm:ss"));
        q.setParameter("dtFim", dtFim.toDateTime(new LocalTime(23,59,59)).toString("yyyy-MM-dd HH:mm:ss"));
        q.setCacheMode(CacheMode.REFRESH);

        if (dtAuditoria.getDT_RowId() != 0)
            q.setParameter("rowid", dtAuditoria.getDT_RowId());
        if (dtAuditoria.getNomeObjeto() != null && !Objects.equals(dtAuditoria.getNomeObjeto(), ""))
            q.setParameter("nomeObjeto", '%' + dtAuditoria.getNomeObjeto() + '%');
        if (dtAuditoria.getIdObjeto() != null && !Objects.equals(dtAuditoria.getIdObjeto(), ""))
            q.setParameter("idObjeto", '%' + dtAuditoria.getIdObjeto() + '%');
        if (dtAuditoria.getOperacao() != null && !Objects.equals(dtAuditoria.getOperacao(), ""))
            q.setParameter("operacao", '%' + dtAuditoria.getOperacao() + '%');
        if (dtAuditoria.getDtHrEvento() != null && !Objects.equals(dtAuditoria.getDtHrEvento(), ""))
            q.setParameter("dtHrEvento", '%' + dtAuditoria.getDtHrEvento() + '%');
        if (dtAuditoria.getUsuario() != null && !Objects.equals(dtAuditoria.getUsuario(), ""))
            q.setParameter("usuario", '%' + dtAuditoria.getUsuario() + '%');

        return new DataTablesHelper<>(DtAuditoria.class).getDatatablesFromRawObjectArray(q.list(), dtAuditoria.isEmpty(), start,
                length, this.countAll(consultaAll, dtInicio, dtFim));
    }

    public int countAll(String consulta, LocalDate dtInicio, LocalDate dtFim) {
        return this.session.createSQLQuery(consulta)
                .setParameter("dtInicio", dtInicio.toDateTimeAtStartOfDay().toString("yyyy-MM-dd HH:mm:ss"))
                .setParameter("dtFim", dtFim.toDateTime(new LocalTime(23,59,59)).toString("yyyy-MM-dd HH:mm:ss"))
                .list().size();
    }

}
