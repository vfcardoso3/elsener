/**
 * User: Vinicius Cardoso
 * Date: 02/04/14
 * Time: 08:23
 */

"use strict";

var TRF = window.TRF || {};

TRF.Sync = function($){
    return {
        defineAjaxSetupAndEvents: function() {
            $(document).ajaxSend(function(ev, jqhr, settings) {
                Pace.restart();
                //TRF.View.displayLoadingIcon(true);
            });
            //$(document).ajaxSend(function(ev, jqhr, settings) {
            //    TRF.View.displayLoadingIcon(true);
            //});
            //$(document).ajaxComplete(function(ev, jqhr, settings) {
            //    TRF.View.displayLoadingIcon(false);
            //});
            $.ajaxSetup({
                statusCode: {
                    400: function() {
                        TRF.Util.alertError(null, "Tempo limite de sessão expirado:::Pressione F5 para atualizar a página e efetuar novo login.");
                    },
                    404: function(data) {
                        TRF.Util.alertDataError(null, data);
                    },
                    500: function(data) {
                        TRF.Util.alertDataError(null, data);
                    }
                }
            });
        },
        changeLoggedUserPassword: function(newPasswd1,newPasswd2) {
            var valida = TRF.Util.checkPassword(newPasswd1, newPasswd2);
            if (valida) {
                $("#form-change-logged-user-password").submit();
            } else {
                $("#alerta_senha").html(
                    '<div class="alert alert-danger">' +
                        '<span>Senhas divergentes ou vazias.</span>' +
                        '<a href="#" class="close xbtn" data-dismiss="alert">&times;</a>' +
                        '</div>');
            }
        },
        changeUserPassword: function(passwdMap) {
            $("#send-new-passwd").click(function (e) {
                e.preventDefault();
                passwdMap.newPasswd1 = (typeof passwdMap.newPasswd1 === 'function') ? passwdMap.newPasswd1() : passwdMap.newPasswd1;
                passwdMap.newPasswd2 = (typeof passwdMap.newPasswd2 === 'function') ? passwdMap.newPasswd2() : passwdMap.newPasswd2;
                var valida = TRF.Util.checkPassword(passwdMap.newPasswd1, passwdMap.newPasswd2);
                if (valida) {
                    $("#form-change-user-password").submit();
                } else {
                    $("#alerta").html(
                        '<div class="alert alert-danger">' +
                            '<span>Existe divergência entre as senhas digitadas.</span>' +
                            '<a href="#" class="close xbtn" data-dismiss="alert">&times;</a>' +
                            '</div>');
                }
            });
        }
    }
}($);
