/**
 * User: Vinicius Cardoso
 * Date: 02/04/14
 * Time: 08:23
 */

"use strict";

var TRF = window.TRF || {};

TRF.Event = function ($) {
    return {
        setChangeMyPasswordModalEvent: function () {
            $("#send-usuario-new-password").on('click', function (e) {
                e.preventDefault();
                var newPasswd1 = $("#senha1-usulogado").val();
                var newPasswd2 = $("#senha2-usulogado").val();
                TRF.Sync.changeLoggedUserPassword(newPasswd1, newPasswd2);
            });
        },
        setAlertExibitionEvent: function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "0",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            if ($(".alert")[0]) {
                var $boxMsg = $(".alert:eq(0)");
                $boxMsg.find(".texto").each(function(i, e){
                    var texto = $.trim($(e).text());
                    if (texto !== "") {
                        var toastrType = $boxMsg.attr('data-toastr-type') !== "" ? $boxMsg.attr('data-toastr-type') : "error";
                        if (TRF.Util.containsString(texto,': ')) {
                            var tituloFull = texto.split(': ')[0];
                            var titulo = tituloFull.split('.')[tituloFull.split('.').length - 1];
                            toastr[toastrType](texto, titulo);
                        } else {
                            toastr[toastrType](texto);
                        }
                    }
                });
                $boxMsg.remove();
            }
        },
        setChangeOtherPasswordModalEvent: function() {
            if ($(".btn-change-passwd")[0]) {
                $(".btn-change-passwd").click(function () {
                    //Ao clicar, adiciona o id ao campo hidden do modal
                    $("#change-passwd-id").val($(this).attr('id'));
                });
                var newPasswd1 = $("#senha1").val();
                var newPasswd2 = $("#senha2").val();
                TRF.Sync.changeUserPassword(newPasswd1, newPasswd2);
            }
        },
        setDataTablesDefaults: function() {
            $.extend( true, $.fn.dataTable.defaults, {
                "language": {
                    "url": TRF.Util.getBaseUrl("/assets/plugins/datatables/datatables.i18n.pt-br.lang")
                },
                "pageLength": 9
            });
        }
    }
}($);