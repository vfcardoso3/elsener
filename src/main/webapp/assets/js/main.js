/**
 * User: Vinicius Cardoso
 * Date: 01/04/14
 * Time: 13:59
 */

"use strict";

//Call on page ready
(function(){
    TRF.Sync.defineAjaxSetupAndEvents();
    TRF.View.setMomentJsLocalePt();
    TRF.Event.setChangeMyPasswordModalEvent();
    TRF.Event.setAlertExibitionEvent();
    TRF.Event.setDataTablesDefaults();
    TRF.View.applySelectPlugins();
    TRF.View.applyNumericPlugins();
    TRF.View.applyDateTimePlugins();
})();