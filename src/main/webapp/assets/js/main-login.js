/**
 * User: Vinicius Cardoso
 * Date: 01/04/14
 * Time: 13:59
 */

$(function () {
    //Mostra mensagens de erro, se existirem
    if ($(".alert")[0]) {
        var boxMsg = $(".alert:eq(0)");
        ($.trim(boxMsg.find(".texto").text()) !== "") ? boxMsg.show() : boxMsg.hide();
    }
});