<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="<c:url value="/painel"/>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>${environment.get("config.nome_sistema.acronim")}</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">${environment.get("config.nome_sistema.regular")}<b>${environment.get("config.nome_sistema.bold")}</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="<c:url value="/painel"/>" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Alternar navegação</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="<c:url value="/painel"/>" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="<c:url value="/assets/img/default_user.gif"/>" class="user-image" alt="Imagem do usuário"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">${userSession.usuario.nome}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                        <img src="<c:url value="/assets/img/default_user.gif"/>" class="img-circle" alt="Imagem do usuário" />
                        <p>
                            ${userSession.usuario.nome}
                            <small>${userSession.usuario.login}</small>
                        </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#change-my-password-modal" data-toggle="modal" class="btn btn-sm btn-default btn-flat">
                                    <i class="fa fa-lock"></i> Alterar senha
                                </a>
                            </div>
                            <div class="pull-right">
                                <a href="<c:url value="/painel/logout"/>" class="btn btn-sm btn-default btn-flat">
                                    <i class="fa fa-sign-out"></i>Sair
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>