<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<c:url value="/assets/img/default_user.gif"/>" class="img-circle" alt="Imagem do usuário"/>
            </div>
            <div class="pull-left info">
                <p>${userSession.usuario.nome}</p>
                <!-- Status -->
                <a href="<c:url value="#"/>"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">MENU PRINCIPAL</li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Configurações</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class=""><!--active?-->
                        <a href="<c:url value="/painel/auditoria/list"/>"><i class='fa fa-user-secret'></i>
                            <span>Auditoria</span>
                        </a>
                    </li>
                    <li class=""><!--active?-->
                        <a href="<c:url value="/painel/usuario/list"/>"><i class='fa fa-user'></i>
                            <span>Usuários</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>