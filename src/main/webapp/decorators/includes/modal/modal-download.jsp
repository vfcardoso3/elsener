<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%--
  User: Vinicius
  Date: 22/07/14
  Time: 22:25
--%>
<div class="modal fade" id="modal-download" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Download de arquivo</h3>
            </div>
            <div class="modal-body">
                <p>Por favor, aguarde enquanto o sistema prepara seu download...</p>
                <br>
                <br>
                <div class="text-center">
                    <img src="<c:url value="/assets/img/loading.gif"/>"/>
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>