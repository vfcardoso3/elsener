<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%--
  User: Vinicius
  Date: 22/07/14
  Time: 22:25
--%>
<div class="modal fade" id="change-my-password-modal" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Alterar sua senha</h3>
            </div>
            <form method="post" id="form-change-logged-user-password" action="<c:url value="/painel/change-logged-user-password"/>">
                <div class="modal-body">
                    <div id="alerta_senha"></div>
                    <input type="hidden" name="usuario.id" value="${userSession.usuario.id}"/>

                    <div class="form-group-sm">
                        <label class="small control-label">Senha atual</label>
                        <input id="currPassword" type="password" name="currPassword" class="form-control no-caps">
                    </div>
                    <div class="form-group-sm">
                        <label class="small control-label">Nova senha</label>
                        <input id="senha1-usulogado" type="password" name="newPassword" class="form-control no-caps">
                    </div>
                    <div class="form-group-sm">
                        <label class="small control-label">Confirmação de senha</label>
                        <input id="senha2-usulogado" type="password" class="form-control no-caps">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default btn-flat" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-sm btn-primary btn-flat" id="send-usuario-new-password">Salvar nova senha</button>
                </div>
            </form>
        </div>
    </div>
</div>