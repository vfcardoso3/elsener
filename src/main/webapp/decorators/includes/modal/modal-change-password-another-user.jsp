<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%--
  User: Vinicius
  Date: 22/07/14
  Time: 22:25
--%>
<div class="modal fade" id="change-password-another-user-modal" >
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Alterar senha</h3>
            </div>
            <form method="post" id="form-change-user-password" action="<c:url value="/painel/usuario/change-password"/>">
                <div class="modal-body">
                    <div id="alerta"></div>
                    <input type="hidden" id="change-passwd-id" name="usuario.id" value="" />
                    <div class="form-group-sm">
                        <label class="small control-label">Senha</label>
                        <input id="senha1" type="password" name="usuario.senha" class="form-control no-caps">
                    </div>
                    <div class="form-group-sm">
                        <label class="small control-label">Confirmação de senha</label>
                        <input id="senha2" type="password" class="form-control no-caps">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-flat btn-default" data-dismiss="modal">Fechar</button>
                    <button class="btn btn-sm btn-flat btn-primary" id="send-new-passwd" type="submit">Salvar nova senha</button>
                </div>
            </form>
        </div>
    </div>
</div>