<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%--
  User: Vinicius
  Date: 22/07/14
  Time: 22:25
--%>
<div class="modal fade" id="modal-error" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close xbtn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Ocorreu um erro!</h3>
            </div>
            <div class="modal-body">
                <span><strong>Message: </strong><span id="error-message" style="text-decoration: underline"></span></span><br>
                <span><strong>StackTrace: </strong><span id="error-stacktrace"></span></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>