<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>

<div class="modal fade" id="modal-view-auditoria">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close xbtn" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
                <span class="modal-title">Detalhamento de auditoria #<span id="mva-id"></span></span>
            </div>
            <div class="modal-body" id="mva-container">

            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary btn-flat" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
