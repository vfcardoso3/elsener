<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%--
  User: Vinicius
  Date: 22/07/14
  Time: 22:25
--%>
<div class="modal fade" id="modal-iframe" >
    <div class="modal-dialog modal-xxl">
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-left">
                    <h3 class="modal-title"></h3>
                </div>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary btn-sm btn-flat" id="iframe-print-btn">
                        <i class="fa fa-print"></i> Imprimir
                    </button>
                    <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i> Fechar
                    </button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-body" style="background: url('/assets/img/loading.gif') center center no-repeat">
                <iframe id="iframe-print" name="iframe-print" width="100%" height="500px" src="" frameborder="0" align="center"></iframe>
            </div>
        </div>
    </div>
</div>