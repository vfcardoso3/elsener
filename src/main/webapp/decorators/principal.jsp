<%--
  Created by IntelliJ IDEA.
  User: vfcar
  Date: 16/07/2017
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${environment.get("config.nome_sistema")} | <decorator:title/></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico"/>" type="image/x-icon">
    <link rel="icon" href="<c:url value="/assets/img/favicon.ico"/>" type="image/x-icon">

    <!-- Bootstrap 3.3.4 -->
    <link href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<c:url value="/assets/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<c:url value="/assets/css/ionicons.min.css"/>" rel="stylesheet" type="text/css" />

    <!-- Plugins and Libraries -->
    <link href="<c:url value="/assets/css/lib/multi-select.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/css/lib/jquery.webui-popover.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/datatables/dataTables.bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/jQuery-contextMenu/jquery.contextMenu.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/css/lib/bootstrap-datetimepicker.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/toastr/toastr.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/select2/select2.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/plugins/pace/pace.css"/>" rel="stylesheet">

    <!-- Theme style -->
    <link href="<c:url value="/assets/css/AdminLTE.min.css"/>" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/skins/skin-red.min.css"/>" rel="stylesheet" type="text/css" />

    <!-- App Style -->
    <link href="<c:url value="/assets/css/main.css"/>" rel="stylesheet" type="text/css" />

    <!-- Local page CSS -->
    <decorator:getProperty property="page.local_style"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-red sidebar-collapse sidebar-mini">
<%@include file="/decorators/includes/modal/modal-error.jsp" %>
<%@include file="/decorators/includes/modal/modal-error-blank.jsp" %>
<%@include file="/decorators/includes/modal/modal-change-logged-password.jsp" %>
<%@include file="/decorators/includes/modal/modal-download.jsp" %>
<%@include file="/decorators/includes/modal/modal-iframe.jsp" %>

<!-- Local page MODAL -->
<decorator:getProperty property="page.local_modal"/>
<div class="wrapper">

    <%@include file="includes/layout/layout-top-menu.jsp" %>

    <%@include file="includes/layout/layout-left-sidebar.jsp" %>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class="pull-left">
                <decorator:getProperty property="page.local_title"/>
                <small><decorator:getProperty property="page.local_subtitle"/></small>
            </h1>
            <div class="pull-right">
                <decorator:getProperty property="page.local_controls"/>
            </div>
            <div class="clearfix"></div>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="alert ${classeAlerta != null ? classeAlerta : 'alert-danger'}" style="display: none" data-toastr-type="${toastrType}">
                <a href="#" class="close xbtn" data-dismiss="alert">&times;</a>
                <span class="texto">${alerta}${causa}</span>
                <c:if test="${not alerta eq ''}">
                    <br/>
                </c:if>
                <c:forEach var="error" items="${errors}">
                    <span class="texto">${error.category} - ${error.message}</span><br/>
                </c:forEach>
            </div>

            <decorator:body/>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Ambiente: ${environment.get("config.nome_ambiente")}
        </div>
        <!-- Default to the left -->
        <span>Versão: 1.0.0</span>
    </footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="<c:url value="/assets/js/lib/jquery.min.js"/>"></script>

<!-- Bootstrap 3.3.2 JS -->
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>

<!-- Plugins and Libraries -->
<!--[if lt IE 9]>
<script src="<c:url value="/assets/js/lib/bootstrap-datetimepicker-retrocompatibility.js"/>"></script>
<![endif]-->
<script src="<c:url value="/assets/js/lib/moment.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.maskedinput.min.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.multi-select.js"/>"></script>
<script src="<c:url value="/assets/plugins/datatables/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/datatables/dataTables.bootstrap.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/jQuery-contextMenu/jquery.contextMenu.min.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.quicksearch.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.autonumeric.js"/>"></script>
<script src="<c:url value="/assets/js/lib/typeahead.bundle.min.js"/>"></script>
<script src="<c:url value="/assets/js/lib/bootstrap-datetimepicker.js"/>"></script>
<script src="<c:url value="/assets/js/lib/highcharts.js"/>"></script>
<script src="<c:url value="/assets/js/lib/highcharts-more.js"/>"></script>
<script src="<c:url value="/assets/js/lib/exporting.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.fileDownload.js"/>"></script>
<script src="<c:url value="/assets/js/lib/ResizeSensor.js"/>"></script>
<script src="<c:url value="/assets/js/lib/shortcut.js"/>"></script>
<script src="<c:url value="/assets/js/lib/jquery.webui-popover.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/toastr/toastr.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/select2/select2.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/select2/i18n/pt-BR.js"/>"></script>
<script src="<c:url value="/assets/plugins/slimScroll/jquery.slimscroll.min.js"/>"></script>
<script src="<c:url value="/assets/plugins/pace/pace.js"/>"></script>

<!-- AdminLTE App -->
<script src="<c:url value="/assets/js/app.min.js"/>" type="text/javascript"></script>

<!-- TRF App -->
<script src="<c:url value="/assets/js/main-view.js"/>"></script>
<script src="<c:url value="/assets/js/main-sync.js"/>"></script>
<script src="<c:url value="/assets/js/main-utils.js"/>"></script>
<script src="<c:url value="/assets/js/main-event.js"/>"></script>
<script src="<c:url value="/assets/js/main.js"/>"></script>

<!-- Scripts locais da página decorada -->
<decorator:getProperty property="page.local_script"/>
</body>
</html>