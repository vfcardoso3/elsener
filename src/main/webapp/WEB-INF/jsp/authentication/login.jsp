<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${environment.get("config.nome_sistema")} | Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<c:url value="/assets/img/favicon.ico"/>" type="image/x-icon">
    <link rel="icon" href="<c:url value="/assets/img/favicon.ico"/>" type="image/x-icon">

    <!-- Bootstrap 3.3.4 -->
    <link href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<c:url value="/assets/css/AdminLTE.min.css"/>" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/main.css"/>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/painel">${environment.get("config.nome_sistema.regular")}<b>${environment.get("config.nome_sistema.bold")}</b></a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Digite suas credenciais</p>
        <div class="alert alert-danger" style="display: none">
            <a class="close xbtn" data-dismiss="alert" href="#">&times;</a>
            <span class="texto">${error}</span>
        </div>
        <form action="<c:url value="/painel/authenticate"/>" method="post">
            <input type="hidden" value="<%=request.getQueryString()%>" name="queryString"/>
            <div class="form-group has-feedback">
                <input type="text" class="form-control no-caps" tabindex="1" placeholder="Usuário" name="usuario.login" ${usuarioLogin == null ? "autofocus" : ""} value="${usuarioLogin}" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control no-caps" tabindex="2" placeholder="Senha" name="usuario.senha" ${usuarioLogin != null ? "autofocus" : ""} />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8 forgot-password pull-left">
                    <a href="<c:url value="/painel/forgot-password"/>" tabindex="4">Esqueceu sua senha?</a>
                </div>
                <div class="col-xs-4 pull-right">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" tabindex="3">Entrar</button>
                </div><!-- /.col -->
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="<c:url value="/assets/plugins/jQuery/jQuery-2.1.4.min.js"/>"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<!-- App -->
<script src="<c:url value="/assets/js/main-login.js"/>"></script>
</body>
</html>