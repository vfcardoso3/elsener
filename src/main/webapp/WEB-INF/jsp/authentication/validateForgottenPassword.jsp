<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<<%--
  Created by IntelliJ IDEA.
  User: Vinicius Cardoso
  Date: 16/07/17
  Time: 12:26
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${environment.get("config.nome_sistema")} | Esqueci minha senha</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<c:url value="/assets/css/AdminLTE.min.css"/>" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/skins/skin-blue.min.css"/>" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/assets/css/main.css"/>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="/painel">${environment.get("config.nome_sistema.regular")}<b>${environment.get("config.nome_sistema.bold")}</b></a>s
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <c:if test="${message != null or errors != null}">
            <div class="alert ${clazz}">
                <a href="#" class="close xbtn" data-dismiss="alert">&times;</a>
                <span class="texto">${message}</span>
                <c:if test="${message != ''}">
                    <br/>
                </c:if>
                <c:forEach var="error" items="${errors}">
                    <span class="texto">${error.category} - ${error.message}</span><br/>
                </c:forEach>
            </div>
        </c:if>

        <form role="form" method="POST" action="<c:url value="/painel/validate-forgotten-password"/>">
            <input type="hidden" name="usuario.forgotPasswordKey" value="<%=request.getQueryString()%>">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <label class="small control-label" for="passwd1">Digite sua nova senha</label>
                        <input type="password" class="form-control" id="passwd1" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group has-feedback">
                        <label class="small control-label" for="passwd2">Repita a nova senha</label>
                        <input type="password" class="form-control" id="passwd2" name="usuario.senha" required>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <a href="<c:url value="/painel/login"/>" class="btn btn-default btn-sm btn-flat">Cancelar</a>
                        <button type="submit" class="btn btn-primary btn-sm btn-flat">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="<c:url value="/assets/plugins/jQuery/jQuery-2.1.4.min.js"/>"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<c:url value="/assets/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<!-- App -->
<script type="text/javascript">
    (function(){
        $('button[type=submit]').on('click', function(e){
            e.preventDefault();
            var $passwd1 = $('#passwd1').val();
            var $passwd2 = $('#passwd2').val();
            if($passwd1 === $passwd2 && $passwd1.length >= 6) {
                $('form').submit();
            } else {
                $('.login-box-body').prepend('<div class="alert alert-danger">' +
                        '<a href="#" class="close xbtn" data-dismiss="alert">&times;</a>' +
                        'Senhas divergentes ou inválidas. Crie uma senha com 6 ou mais caracteres.' +
                        '</div>')
            }
        });
    })();
</script>
</body>
</html>