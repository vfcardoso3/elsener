<%--
  Created by IntelliJ IDEA.
  User: Vinicius
  Date: 25/07/14
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cadastrar usuário</title>
</head>

<content tag="local_title">Usuários</content>
<content tag="local_subtitle">Cadastro</content>
<content tag="local_controls"></content>

<body>
<div class="box">
    <div class="box-title with-border"></div>
    <div class="box-tools pull-right"></div>
    <div class="box-body">
        <form role="form" method="POST" action="<c:url value="/painel/usuario/add"/>" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group-sm">
                        <label class="small control-label" for="nome">Nome</label>
                        <input type="text" class="form-control no-caps" id="nome" name="usuario.nome" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group-sm">
                        <label class="small control-label" for="email">E-mail</label>
                        <input type="email" class="form-control no-caps" id="email" name="usuario.email" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label">Usuário ativo?</label>
                        <div class="form-control">
                            <label class="radio-inline">
                                <input type="radio" name="usuario.ativo" id="ativo-true" value="true" checked> Sim
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="usuario.ativo" id="ativo-false" value="false"> Não
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group-sm">
                        <label class="small control-label" for="login">Login</label>
                        <input type="text" class="form-control no-caps" id="login" name="usuario.login" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label" for="senha">Senha <small><a href="#" id="gerar-senha">(random!)</a></small></label>
                        <input type="text" class="form-control no-caps" id="senha" name="usuario.senha" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label" for="papel">Papel</label>
                        <select name="usuario.role" class="form-control select2" id="papel">
                            <c:forEach var="papel" items="${papelList}">
                                <option value="${papel.id}">${papel.toString().toUpperCase()}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group-sm">
                        <label class="small control-label" for="email">E-mails a receber cópia <span class="text-muted">(separados por vírgula)</span></label>
                        <input type="text" class="form-control no-caps" id="emailCopia" name="emailCopia" disabled>
                    </div>
                </div>
                <div class="col-md-2">
                    <label class="small control-label" for="enviarEmail" style="margin-top: 25px">
                        <input type="checkbox" id="enviarEmail" name="enviarEmail" value="true" checked="checked"> Enviar e-mail?
                    </label>
                </div>
                <div class="col-md-2">
                    <label class="small control-label" for="enviarCopia" style="margin-top: 25px">
                        <input type="checkbox" id="enviarCopia" name="enviarCopia" value="true"> Enviar cópia?
                    </label>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <a href="<c:url value="/painel/usuario/list"/>" class="btn btn-sm btn-link btn-flat">Cancelar</a>
                        <button type="submit" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
<content tag="local_style"></content>
<content tag="local_script">
    <script type="text/javascript">
        (function(){
            var $enviarCopia = $('#enviarCopia');
            var $emailCopia = $('#emailCopia');
            $enviarCopia.on('change', function(){
                if($enviarCopia.is(':checked')) {
                    $emailCopia.removeAttr('disabled');
                } else {
                    $emailCopia.attr('disabled','disabled');
                }
            });
            TRF.View.maskInputCelularNonoDigito($("#telefone"));

            $('#gerar-senha').on('click', function(e){
                e.preventDefault();
                $('#senha').val(TRF.Util.generateRandomString(15,true,true,false));
            });
        })()
    </script>
</content>