<%--
  Created by IntelliJ IDEA.
  User: Vinicius
  Date: 25/07/14
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="/WEB-INF/tlds/custom-functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Editar usuário</title>
</head>

<content tag="local_title">Usuários</content>
<content tag="local_subtitle">Edição</content>
<content tag="local_controls"></content>

<body>
<div class="box">
    <div class="box-title with-border"></div>
    <div class="box-tools pull-right"></div>
    <div class="box-body">
        <form role="form" method="POST" action="<c:url value="/painel/usuario/update"/>" enctype="multipart/form-data">
            <input type="hidden" name="usuario.id" value="${usuario.id}"/>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group-sm">
                        <label class="small control-label" for="nome">Nome</label>
                        <input type="text" class="form-control no-caps" id="nome" name="usuario.nome" value="${usuario.nome}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group-sm">
                        <label class="small control-label" for="email">E-mail</label>
                        <input type="email" class="form-control no-caps" id="email" name="usuario.email" value="${usuario.email}" required>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label">Usuário ativo?</label>
                        <div class="form-control">
                            <label class="radio-inline">
                                <input type="radio" name="usuario.ativo" ${usuario.ativo ? 'checked="checked"' : ''}
                                       id="ativo-true" value="true"> Sim
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="usuario.ativo" ${not usuario.ativo ? 'checked="checked"' : ''}
                                       id="ativo-false" value="false"> Não
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label" for="login">Login</label>
                        <input type="text" class="form-control no-caps" id="login" name="usuario.login" value="${usuario.login}" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group-sm">
                        <label class="small control-label" for="senha">Senha</label>
                        <input type="password" class="form-control no-caps" id="senha" name="usuario.senha" value="${usuario.senha}" disabled="disabled">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group-sm">
                        <label class="small control-label" for="papel">Papel</label>
                        <select name="usuario.role" class="form-control select2" id="papel">
                            <c:forEach var="papel" items="${papelList}">
                                <option value="${papel.id}" ${usuario.role.id eq papel.id ? "selected='selected'" : ""}>${papel.toString().toUpperCase()}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <a href="<c:url value="/painel/usuario/list"/>" class="btn btn-sm btn-link btn-flat">Cancelar</a>
                        <button type="submit" class="btn btn-sm btn-primary btn-flat">Atualizar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
<content tag="local_style"></content>
<content tag="local_script">
    <script type="text/javascript">
        (function(){
            TRF.View.maskInputCelularNonoDigito($("#telefone"));
        })()
    </script>
</content>