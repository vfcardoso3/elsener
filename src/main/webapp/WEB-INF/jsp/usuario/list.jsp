<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Usuários</title>
</head>

<content tag="local_title">Usuários</content>
<content tag="local_subtitle">Listagem</content>
<content tag="local_controls">
    <div class="pull-right form-inline top-controls">
        <div class="btn-group btn-group-sm">
            <a href="<c:url value="/painel/usuario/create"/>" class="btn btn-default btn-flat">
                <span class="glyphicon glyphicon-plus-sign small"></span> Novo Usuário
            </a>
            <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>
                    <a href="#" id="exportar-listagem" class="small">
                        <span class="glyphicon glyphicon-export"></span> Exportar listagem
                    </a>
                </li>
            </ul>
        </div>
    </div>
</content>

<body>
<div class="row">
    <div class="col-lg-12 panel-grid">
        <div class="box">
            <div class="box-body">
                <table id="dt-usuarios" class="table table-striped table-bordered table-hover small" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 5%">ID</th>
                        <th style="width: 5%">Status</th>
                        <th style="width: 40%">Nome</th>
                        <th style="width: 10%">Login</th>
                        <th style="width: 30%">E-mail</th>
                        <th style="width: 10%">Papel</th>
                    </tr>
                    <tr class="filters">
                        <th data-number-only></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<content tag="local_style">
    <style type="text/css">
    </style>
</content>
<content tag="local_script">
    <script type="text/javascript">
        $(function ($) {
            var $tb = $('#dt-usuarios');
            TRF.View.generateColumnFilters($tb);

            var $grid = $tb.DataTable( {
                "processing": true,
                "serverSide": true,
                "orderCellsTop": true,
                "lenghtChange": false,
                "dom": 'rtip',
                "ajax": {
                    "url": TRF.Util.getBaseUrl("/painel/usuario/list.json"),
                    "type": "POST",
                    "data": function(d) {
                        return $.extend({}, d, {
                            'dtUsuario.rowid': $('.dt-filter:eq(0)').val(),
                            'dtUsuario.status': $('.dt-filter:eq(1)').val(),
                            'dtUsuario.nome': $('.dt-filter:eq(2)').val(),
                            'dtUsuario.login': $('.dt-filter:eq(3)').val(),
                            'dtUsuario.email': $('.dt-filter:eq(4)').val(),
                            'dtUsuario.papel': $('.dt-filter:eq(5)').val()
                        });
                    }
                },
                "columns": [
                    { "data": "rowid" },
                    { "data": "status" },
                    { "data": "nome" },
                    { "data": "login" },
                    { "data": "email" },
                    { "data": "papel" }
                ],
                "order": [ 2, "asc" ]
            });

            TRF.View.addColumnFiltersListeners($grid);

            $('#exportar-listagem').on('click', function(e){
                e.preventDefault();
                if(confirm('Tem certeza de que deseja baixar a listagem atual em Excel?')) {
                    var postData = Object.create($grid.ajax.params());
                    TRF.View.downloadFileAjax(TRF.Util.getBaseUrl('/painel/usuario/list/export?') + $.param(postData));
                }
            });

            var contextMenuOpts = {
                selector: '.dataTable td:not([colspan])',
                items: {
                    view: {
                        name: "Detalhar",
                        icon: "fa-search",
                        disabled: false,
                        callback: function (key, opt) {
                            var id = opt.$trigger.parent().find('td:eq(0)').text();
                            Pace.track(function(){
                                $.ajax({
                                    type: 'post',
                                    url: TRF.Util.getBaseUrl("/painel/usuario/view.json"),
                                    data: { id: id },
                                    dataType: 'json',
                                    success: function(data) {
                                        //TRF.Modal.fillModalUsuario(data);
                                        alert('modal usuário');
                                        TRF.View.showModal('#modal-view-usuario');
                                    },
                                    error: function(data) {
                                        TRF.Util.alertDataError(this.url, data);
                                    }
                                });
                            });
                        }
                    },
                    changePassword: {
                        name: "Alterar senha",
                        icon: "fa-lock",
                        disabled: false,
                        callback: function (key, opt) {
                            TRF.View.showModal('#change-password-another-user-modal');
                            var id = opt.$trigger.parent().find('td:eq(0)').text();
                            $("#change-passwd-id").val(id);
                            TRF.Sync.changeUserPassword({
                                'newPasswd1': function() { return $('#senha1').val() },
                                'newPasswd2': function() { return $('#senha2').val() }
                            });
                        }
                    },
                    resendWelcomeEmail: {
                        name: "Reenviar e-mail de boas-vindas",
                        icon: "fa-send",
                        disabled: false,
                        callback: function (key, opt) {
                            var id = opt.$trigger.parent().find('td:eq(0)').text();
                            if(confirm('Tem certeza que deseja reenviar o e-mail de boas-vindas?')) {
                                Pace.track(function(){
                                    $.ajax({
                                        type: 'get',
                                        url: TRF.Util.getBaseUrl("/painel/usuario/resend-welcome-email/" + id),
                                        dataType: 'html',
                                        success: function() {
                                            toastr.success("E-mail enviado com sucesso!");

                                        },
                                        error: function(data) {
                                            TRF.Util.alertDataError(this.url, data);
                                        }
                                    });
                                });
                            } else {
                                return false;
                            }
                        }
                    },
                    edit: {
                        name: "Editar",
                        icon: "fa-edit",
                        disabled: false,
                        callback: function (key, opt) {
                            var id = opt.$trigger.parent().find('td:eq(0)').text();
                            window.open(TRF.Util.getBaseUrl("/painel/usuario/edit/" + id));
                        }
                    },
                    remove: {
                        name: "Remover",
                        icon: "fa-trash",
                        disabled: false,
                        callback: function (key, opt) {
                            var id = opt.$trigger.parent().find('td:eq(0)').text();
                            var nomeUsuario = opt.$trigger.parent().find('td:eq(2)').text();
                            if(confirm('Tem certeza de que deseja remover o usuário ' + nomeUsuario + '?')) {
                                Pace.track(function(){
                                    $.ajax({
                                        type: 'get',
                                        url: TRF.Util.getBaseUrl("/painel/usuario/remove/" + id),
                                        dataType: 'html',
                                        success: function() {
                                            toastr.success("Usuário removido com sucesso!");
                                            $grid.ajax.reload(null, false);

                                        },
                                        error: function(data) {
                                            TRF.Util.alertDataError(this.url, data);
                                        }
                                    });
                                });

                            }
                        }
                    }
                }
            };
            $.contextMenu(contextMenuOpts);
        });
    </script>
</content>
<content tag="local_modal">
    <%@include file="../../../decorators/includes/modal/modal-change-password-another-user.jsp"%>
    <%--<%@include file="../../../decorators/includes/modal/modal-view-usuario.jsp"%>--%>
</content>