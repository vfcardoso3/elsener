
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
    <div class="row">
        <div class="col-lg-12">
            <div class="app-divider-title basicos">Dados da Operação</div>
            <div class="app-divider">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group-sm">
                            <label class="small" for="nomeObjeto">Nome do Objeto</label>
                            <div class="form-control" id="nomeObjeto">${auditoria.nomeObjeto}</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group-sm">
                            <label class="small" for="idObjeto">ID do Objeto</label>
                            <div class="form-control" id="idObjeto">${auditoria.idObjeto}</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group-sm">
                            <label class="small" for="operacao">Operação</label>
                            <div class="form-control" id="operacao">${auditoria.operacao}</div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group-sm">
                            <label class="small" for="dtHrEvento">Dt/Hr Evento</label>
                            <div class="form-control" id="dtHrEvento">${auditoria.dtHrEvento.toString("dd/MM/yyyy HH:mm:ss")}</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group-sm">
                            <label class="small" for="usuario">Usuário</label>
                            <div class="form-control" id="usuario">${auditoria.usuario.nome}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="position: relative;">
                <div class="app-divider-title documentos">Dados da Operação</div>
                <label class="checkbox pull-right app-divider-check small" for="exibirDadosInalterados">
                    <input type="checkbox" id="exibirDadosInalterados" value="true" style="margin-top: 1px !important"> Exibir dados inalterados?
                </label>
            </div>
            <div class="app-divider">
                <div class="row">
                    <div class="col-md-12">
                        <div id="mva-diff-visual"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<content tag="local_script">
    <script type="text/javascript" src="<c:url value="/assets/plugins/jsondiffpatch/jsondiffpatch.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/assets/plugins/jsondiffpatch/jsondiffpatch-formatters.min.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/assets/plugins/jsondiffpatch/html.css"/>" type="text/css" />
    <script type="text/javascript">
        (function(){
            var left = ${auditoria.jsonObjetoOld == null ? "{}" : auditoria.jsonObjetoOld};
            var right = ${auditoria.jsonObjetoNovo};
            var delta = jsondiffpatch.diff(left, right);

            if (delta == undefined) {
                document.getElementById('mva-diff-visual').innerHTML = '<small>Não houve modificações.</small>';
            } else {
                document.getElementById('mva-diff-visual').innerHTML = jsondiffpatch.formatters.html.format(delta, left);
            }

            var $checkToggleUnchanged = $('#exibirDadosInalterados');
            $checkToggleUnchanged.on('change', function(){
                if($checkToggleUnchanged.is(':checked')) {
                    jsondiffpatch.formatters.html.showUnchanged();
                } else {
                    jsondiffpatch.formatters.html.hideUnchanged();
                }
            }).trigger('change');
        })();
    </script>
</content>