<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Auditoria</title>
</head>

<content tag="local_title">Auditoria</content>
<content tag="local_subtitle">Listagem</content>
<content tag="local_controls">
    <div class="pull-right form-inline top-controls">
        <div class="form-group">
            <label for="dtInicio" class="small">Data inicial</label>
            <input type="text" class="form-control input-sm" id="dtInicio" value="${data1}">
        </div>
        <div class="form-group">
            <label for="dtFim" class="small">Data final</label>
            <input type="text" class="form-control input-sm" id="dtFim" value="${data2}">
        </div>
        <div class="form-group">
            <button class="btn btn-info btn-flat btn-sm" id="filtrar"><span class="glyphicon glyphicon-ok small"></span> Aplicar</button>
        </div>
        <div class="btn-group btn-group-sm">
            <a href="#" id="exportar-listagem" class="btn btn-default btn-flat">
                <span class="glyphicon glyphicon-export"></span> Exportar listagem
            </a>
        </div>
    </div>
</content>

<body>
<div class="row">
    <div class="col-lg-12 panel-grid">
        <div class="box">
            <div class="box-body">
                <table id="dt-auditoria" class="table table-striped table-bordered table-hover small" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="width: 22%">Nome do Objeto</th>
                        <th style="width: 12%">ID do Objeto</th>
                        <th style="width: 22%">Operação</th>
                        <th style="width: 22%">Dt/Hr Evento</th>
                        <th style="width: 22%">Usuário</th>
                    </tr>
                    <tr class="filters">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<content tag="local_style"></content>
<content tag="local_script">
    <script type="text/javascript">
        $(function ($) {
            $('#dtInicio,#dtFim').datetimepicker({locale: 'pt-br', format: 'DD/MM/YYYY'});

            var $tb = $('#dt-auditoria');

            TRF.View.generateColumnFilters($tb);

            var $grid = $tb.DataTable({
                "processing": true,
                "serverSide": true,
                "orderCellsTop": true,
                "lengthChange": false,
                "dom": 'rtip',

                "ajax": {
                    "url": TRF.Util.getBaseUrl("/painel/auditoria/list.json"),
                    "type": "POST",
                    "data": function(d) {
                        return $.extend({}, d, {
                            'dtInicio' : $('#dtInicio').val(),
                            'dtFim' : $('#dtFim').val(),
                            'dtAuditoria.nomeObjeto': $('.dt-filter:eq(0)').val(),
                            'dtAuditoria.idObjeto': $('.dt-filter:eq(1)').val(),
                            'dtAuditoria.operacao': $('.dt-filter:eq(2)').val(),
                            'dtAuditoria.dtHrEvento': $('.dt-filter:eq(3)').val(),
                            'dtAuditoria.usuario': $('.dt-filter:eq(4)').val()
                        });
                    }
                },
                "columns": [
                    { "data": "nomeObjeto" },
                    { "data": "idObjeto" },
                    { "data": "operacao" },
                    { "data": "dtHrEvento", "type": "date", "render": TRF.View.dataTablesDateTimeFormatterWithSecs },
                    { "data": "usuario" }
                ],
                "order": [ 3, "desc" ]
            });

            TRF.View.addColumnFiltersListeners($grid);

            $('#filtrar').on('click', function () {
                $grid.ajax.reload(null, false);
            });

            $('#exportar-listagem').on('click', function(e){
                e.preventDefault();
                if(confirm('Tem certeza de que deseja baixar a listagem atual em Excel?')) {
                    var postData = Object.create($grid.ajax.params());
                    TRF.View.downloadFileAjax(TRF.Util.getBaseUrl('/painel/auditoria/list/export?') + $.param(postData));
                }
            });

            var contextMenuOpts = {
                selector: '.dataTable td:not([colspan])',
                items: {
                    view: {
                        name: "Detalhar",
                        icon: "fa-search",
                        disabled: false,
                        callback: function (key, opt) {
                            var id = opt.$trigger.parents('tr').attr('id');
                            Pace.track(function(){
                                $.ajax({
                                    type: 'post',
                                    url: TRF.Util.getBaseUrl("/painel/auditoria/view.partial"),
                                    data: { id: id },
                                    dataType: 'html',
                                    success: function(data) {
                                        $('#mva-id').text(id);
                                        $('#mva-container').html(data);
                                        TRF.View.showModal('#modal-view-auditoria');
                                    },
                                    error: function(data) {
                                        TRF.Util.alertDataError(this.url, data);
                                    }
                                });
                            });
                        }
                    }
                }
            };
            $.contextMenu(contextMenuOpts);
        });
    </script>
</content>
<content tag="local_modal">
    <%@include file="../../../decorators/includes/modal/modal-view-auditoria.jsp"%>
</content>